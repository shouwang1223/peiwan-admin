<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: wuwu <15093565100@163.com>
// +----------------------------------------------------------------------
namespace api\wxapp\controller;

use app\admin\model\PlayWithModel;
use cmf\controller\RestBaseController;
use think\facade\Db;

class AuthController extends RestBaseController
{

    public $user_info;
    public $user_id;
    public $openid;

    /**
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     */
    public function initialize()
    {
        $openid = $this->request->header('openid');
        if (empty($openid)) {
            $openid = $this->request->param('openid');
        }
        $lat = $this->request->param('lat');
        $lng = $this->request->param('lng');
        $user_info = $this->getUserInfoByOpenid($openid);
        if (!empty($user_info)) {
            $play_with = PlayWithModel::where(['user_id' => $user_info['id']])->find();
            if (!empty($play_with)) {
                $user_info['is_play_with'] = $play_with['status'] == 0 ? 1 : 2;
                $user_info['pw_id'] = $play_with['id'];
                $user_info['checked'] = $play_with['status'] == 1 ? true : false;
                $user_info['play_with_id'] = $play_with['id'];
            } else {
                $user_info['is_play_with'] = 0;
                $user_info['checked'] = false;
                $user_info['play_with_id'] = 0;
            }

            if ($user_info['is_play_with'] == 2 && !empty($lat) && !empty($lng)) {
                PlayWithModel::where(['user_id' => $this->user_id])->update(['lat' => $lat, 'lng' => $lng, 'city' => $this->search_address($lat, $lng)]);
            }

            $user_info['is_member'] = $user_info['member_time'] > time() ? 1 : 0;

            $this->user_info = $user_info;
            $this->user_id = $user_info['id'];
            $this->openid = $openid;
        }
    }

    public function search_address($lat, $lng): string
    {
        $url = "https://apis.map.qq.com/ws/geocoder/v1/?key=JZKBZ-F5R6X-ZHC4W-T7TZC-5V3GV-NZBSX&location=" . $lat . "," . $lng;
        $result = file_get_contents($url);
        $result = json_decode($result, true);
        if ($result['status'] == 0) {
            $run = $result['result']['address_component']['city'];
        } else {
            $run = '';
        }
        return $run;
    }

    public function checkAuth()
    {
        if (empty($this->user_id)) {
            $this->error('请先授权登录');
        }
    }

    /**
     * 根据openid获取用户user_info
     * @param $openid
     * @return array|mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getUserInfoByOpenid($openid)
    {
        if (empty($openid)) $openid = '123';

        $map['openid'] = $openid;
        $user = Db::name('member')->where($map)->select()->toArray();
        if ($user) {
            return $user[0];
        } else {
            return [];
        }
    }
}
