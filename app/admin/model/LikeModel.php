<?php
declare (strict_types = 1);

namespace app\admin\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class LikeModel extends Model
{
    //
    protected $name = 'pw_like';
}
