<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2018 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 秦桦 <309167611@qq.com>
// +----------------------------------------------------------------------
namespace plugins\yun_pian_sms;

use cmf\lib\Plugin;
use Yunpian\Sdk\YunpianClient;

class YunPianSmsPlugin extends Plugin
{
    public $info = [
        'name' => 'YunPianSms',
        'title' => '云片-短信插件',
        'description' => '云片-短信插件',
        'status' => 1,
        'author' => 'QinHua',
        'version' => '1.0'
    ];

    public $hasAdmin = 0;//插件是否有后台管理界面


    // 插件安装
    public function install()
    {
        return true;//安装成功返回true，失败false
    }

    // 插件卸载
    public function uninstall()
    {
        return true;//卸载成功返回true，失败false
    }

    //实现的send_mobile_code钩子方法
    public function sendMobileCode($param)
    {
        $mobile = $param['mobile'];//手机号

        $code = $param['code'];//验证码

        $config = $this->getConfig();

        $expire_minute = intval($config['expire_minute']);

        $expire_minute = empty($expire_minute) ? 1 : $expire_minute;

        $apikey = $config['apikey'];

        $result = false;
        //初始化client,apikey作为所有请求的默认值
        $clnt = YunpianClient::create($apikey);

        $param = [
            YunpianClient::APIKEY => $apikey,
            YunpianClient::MOBILE => $mobile,
            YunpianClient::TEXT   => '【云片网】您的验证码是'.$code.'。过期时间是'.$expire_minute.'分钟,如非本人操作，请忽略本短信',
        ];
        //单条发送，可智能匹配短信模板
        $r = $clnt->sms()->single_send($param);

        $response = $r->data();

        if($r->isSucc()){//发送成功

            $result = [
                'error'=>0,
                'message'=>$response['msg']
            ];

        }else{

            $result = [
                'error'=>$response['code'],
                'message'=>$response['msg']
            ];

        }

        return $result;
    }
}