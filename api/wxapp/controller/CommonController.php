<?php

namespace api\wxapp\controller;

use app\admin\model\ArticleModel;
use app\admin\model\CategoryModel;
use app\admin\model\CommentModel;
use app\admin\model\CommonModel;
use app\admin\model\CouponModel;
use app\admin\model\EarningsModel;
use app\admin\model\MemberModel;
use app\admin\model\OrderAppendModel;
use app\admin\model\OrderModel;
use app\admin\model\PlayWithModel;
use app\admin\model\ShopModel;
use OpenApi\Annotations as OA;
use plugins\weipay\lib\PayController;
use think\facade\Db;
use think\facade\Log;
use think\Model;
use function Composer\Autoload\includeFile;

class CommonController extends AuthController
{
    /**
     * 获取个人信息
     * @OA\Get (
     *     tags={"陪玩小程序"},
     *     path="/wxapp/common/get_user_info",
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function get_user_info()
    {
        $this->checkAuth();

        $map = [];
//        if ($this->user_info['is_play_with'] == 2) {
//            $map[] = ['pw_id', '=', $this->user_info['pw_id']];
//        } else {
//            $map[] = ['user_id', '=', $this->user_id];
//        }
        $map[] = ['user_id', '=', $this->user_id];
        $map[] = ['type', '=', 1];
        $this->user_info['await_order_count'] = OrderModel::where($map)->where('status', '=', 2)->count();
        $this->user_info['ongoing_order_count'] = OrderModel::where($map)->where('status', 'in', [4, 6])->count();
        $this->user_info['complete_order_count'] = OrderModel::where($map)->where('status', '=', 8)->count();
        if ($this->user_info['type'] == 2) $this->user_info['qr_code'] = cmf_get_asset_url(CommonModel::getCodeQrImg(cmf_get_domain() . '/h5/#/pages/index/index?parent_id=' . $this->user_id));
        //优惠券
        $this->user_info['coupon'] = CouponModel::where(['status' => 1, 'user_id' => $this->user_id])->where('end_time', '>', time())->count();

        $this->success('请求成功', $this->user_info);
    }

    /**
     * 分类列表
     * @OA\Get (
     *     tags={"陪玩小程序"},
     *     path="/wxapp/common/find_category_list",
     *     @OA\Parameter(
     *         name="pw_id",
     *         in="query",
     *         description="陪玩ID",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function find_category_list()
    {
        $pw_id = $this->request->param('pw_id');

        $map = [];
        if (!empty($pw_id)) {
            $ids = PlayWithModel::where('id', $pw_id)->value('cate_ids');
            $ids = json_decode($ids, true);
            if (!empty($ids)) $map[] = ['id', 'in', $ids];
        }

        $data = CategoryModel::where($map)->order('list_order asc')->select()->each(function ($item) {
            $item['icon'] = cmf_get_asset_url($item['icon']);
            $item['checked'] = false;
            return $item;
        });

        $this->success('请求成功', $data);
    }

    /**
     * 用户完善手机号
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\db\exception\DbException
     * @OA\Get (
     *     tags={"陪玩小程序"},
     *     path="/wxapp/common/update_user_phone",
     *     @OA\Parameter(
     *         name="phone",
     *         in="query",
     *         description="手机号",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function update_user_phone()
    {
        $this->checkAuth();
        $phone = $this->request->param('phone');

        $res = MemberModel::where(['id' => $this->user_id])->update(['phone' => $phone]);
        if ($res) {
            $this->success('修改成功');
        } else {
            $this->error('修改失败');
        }
    }

    /**
     * 陪玩状态修改
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\db\exception\DbException
     * @OA\Get (
     *     tags={"陪玩小程序"},
     *     path="/wxapp/common/update_play_with",
     *     @OA\Parameter(
     *         name="pw_id",
     *         in="query",
     *         description="陪玩ID",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function update_play_with()
    {
        $this->checkAuth();
        if ($this->user_info['is_play_with'] != 2) $this->error('您还不是陪玩');

        $pw_id = $this->request->param('pw_id');

        $info = PlayWithModel::find($pw_id);

        if (empty($pw_id)) $this->error('陪玩不存在');

        if ($info['status'] == 0) $this->error('您还的陪玩申请还未审核通过');

        $status = $info['status'] == 1 ? 2 : 1;

        $res = PlayWithModel::where(['id' => $this->user_info['pw_id']])->update(['status' => $status]);
        if ($res) {
            $this->success('修改成功', $status == 1 ? true : false);
        } else {
            $this->error('修改失败', $status == 1 ? false : true);
        }
    }

    /**
     * 陪玩信息
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\db\exception\DbException
     * @OA\Get (
     *     tags={"陪玩小程序"},
     *     path="/wxapp/common/find_play_with",
     *     @OA\Parameter(
     *         name="type",
     *         in="query",
     *         description="1推荐 2附近 3新人",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="ID详情",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="status",
     *         in="query",
     *         description="1在线 2离线",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="lat",
     *         in="query",
     *         description="纬度",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="lng",
     *         in="query",
     *         description="经度",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function find_play_with()
    {
        $id = $this->request->param('id');
        $type = $this->request->param('type');
        $status = $this->request->param('status');
        $lat = $this->request->param('lat');
        $lng = $this->request->param('lng');
        $city = $this->request->param('city');
        $sex = $this->request->param('sex');
        $cateId = $this->request->param('cate_id');
        $nickname = $this->request->param('nickname');

        if (empty($lat) || empty($lng)) $this->error('缺少位置信息');

        $Model = new PlayWithModel();
        if (!empty($id)) {
            $data = $Model->field("*,6378.138 * 2 * asin(sqrt(pow(sin((lat * pi() / 180 - " . $lat . " * pi() / 180) / 2),2) + cos(lat * pi() / 180) * cos(" . $lat . " * pi() / 180) * pow(sin((lng * pi() / 180 - " . $lng . " * pi() / 180) / 2),2))) as distance")->find($id);
            if (!empty($data)) {
                //$distance_info = json_decode($this->getDistance([$lat, $lng], [$data['lat'], $data['lng']], 'distance,taxi_fare'), true);
                //$data['distance'] = $distance_info['distance'];
                //$data['pw_trip'] = $distance_info['taxi_fare'];
                $data['distance'] = round($data['distance'], 2);
                $data['thumb'] = CommonModel::getArrImageFullUrl($data['thumb']);
                $data['comment_count'] = CommentModel::where(['pw_id' => $id])->count();
                $data['member_price'] = round($data['price'] * 0.8, 2);
                $data['cate_ids'] = json_decode($data['cate_ids'], true);
            }
        } else {
            $order = 'id';
            $sort = SORT_DESC;

            $map = [];
            if (!empty($nickname)) {
                $map[] = ['nickname', 'like', '%' . $nickname . '%'];
            } else {
                if (!empty($type)) {
                    if ($type == 3) {
                        $map[] = ['update_time', '>', (time() - (86400 * cmf_get_option('set_config')['new_pw_day']))];
                    } elseif ($type == 2) {
                        $order = 'distance';
                        $sort = SORT_ASC;
                    } else {
                        $map[] = ['is_top', '=', 1];
                    }
                }
            }
            if (!empty($status)) {
                $map[] = ['status', '=', $status];
            } else {
                $map[] = ['status', '<>', 0];
            }
            if (!empty($city)) {
                $map[] = ['city', '=', $city];
            }
            if (!empty($sex)) {
                $sex = $sex == 1 ? '男' : '女';
                $map[] = ['sex', '=', $sex];
            }
            $pw_list = $Model->field("*,6378.138 * 2 * asin(sqrt(pow(sin((lat * pi() / 180 - " . $lat . " * pi() / 180) / 2),2) + cos(lat * pi() / 180) * cos(" . $lat . " * pi() / 180) * pow(sin((lng * pi() / 180 - " . $lng . " * pi() / 180) / 2),2))) as distance")
                ->where($map)->select()->each(function ($item) use ($lat, $lng) {
                    $item['distance'] = round($item['distance'], 2);
                    $item['thumb'] = CommonModel::getArrImageFullUrl($item['thumb']);
                    $item['member_price'] = round($item['price'] * 0.8, 2);
                    $item['cate_ids'] = json_decode($item['cate_ids'], true);
                    return $item;
                })->toArray();

            $list = [];
            if (!empty($pw_list)) {
                foreach ($pw_list as $k => $v) {
                    if (!empty($cateId)) {
                        if (in_array($cateId, $v['cate_ids'])) {
                            if ($v['distance'] < 15000) array_push($list, $v);
                        }
                    } else {
                        if ($v['distance'] < 15000) array_push($list, $v);

                    }

                }
                $last_names = array_column($list, $order);
                array_multisort($last_names, $sort, $list);
            }

            $page = $this->request->param('page', 1);
            $data['total'] = count($list);
            $data['per_page'] = 10;
            $data['current_page'] = $page;
            $data['last_page'] = (int)ceil(count($list) / 10);
            $num = ($page - 1) * 10;
            $data['data'] = array_slice($list, $num, 10);
        }
        $this->success('请求成功', $data);
    }

    public function getCateId()
    {
        $id = $this->request->param('id');

        $categoryModel = new CategoryModel();

        $res = $categoryModel->where('id', $id)->find();

        $this->success('请求成功', $res);
    }

    //排行榜
    public function getRanking()
    {

        $city = $this->request->param('city');
        $type = $this->request->param('type');
        $rank = $this->request->param('rank');
        $Model = new PlayWithModel();
        $map = [];

        $map[] = ['a.status', '<>', 0];
        if (!empty($city)) {
            $map[] = ['a.city', '=', $city];
        }
        if ($type == 1) {
            $pw_list = $Model->alias('a')->field("a.thumb,a.nickname,count(b.num) as like_num")
                ->leftJoin('pw_with_like b', 'a.user_id = b.pw_user_id')
                ->where($map)->when($rank, function ($query) use ($rank) {
                    if ($rank == 1) {
                        $query->whereWeek('b.create_time');
                    } else {
                        $query->whereMonth('b.create_time');
                    }
                })->group('b.pw_user_id')->limit(5)->order('like_num desc,id desc')->select()->each(function ($item) {
                    $item['thumb'] = CommonModel::getArrImageFullUrl($item['thumb']);
                    return $item;
                })->toArray();
        } else {
            $pw_list = $Model->alias('a')->field("a.thumb,a.nickname,count(*) as order_num")
                ->leftJoin('pw_order b', 'a.id = b.pw_id')
                ->where($map)->when($rank, function ($query) use ($rank) {
                    if ($rank == 1) {
                        $query->whereWeek('b.create_time');
                    } else {
                        $query->whereMonth('b.create_time');
                    }
                })->group('b.pw_id')->limit(5)->order('order_num desc,a.id desc')->select()->each(function ($item) {
                    $item['thumb'] = CommonModel::getArrImageFullUrl($item['thumb']);
                    return $item;
                })->toArray();
        }

        $this->success('请求成功', $pw_list);
    }

    //查询文章
    public function getArticleId()
    {
        $id = $this->request->param('id');
        $articleModel = new ArticleModel();
        $res = $articleModel->find($id);
        $this->success('请求成功', $res);
    }

    //点赞
    public function update_pay_like()
    {
        $id = $this->request->param('id');
        $uid = $this->request->param('uid');
        $user_id = $this->request->param('user_id');
        $Model = new PlayWithModel();
        $res = $Model->where('id', $id)->inc('like')->update();
        Db::name('pw_with_like')->insert(['user_id' => $uid, 'pw_user_id' => $user_id, 'create_time' => time(), 'num' => 1]);
        $this->success('点赞成功', $res);
    }

    public function getDistance($from, $to, $field = 'distance')
    {
        $from = implode(",", $from);
        $to = implode(",", $to);
        $url = 'https://apis.map.qq.com/ws/direction/v1/driving/?from=' . $from . '&to=' . $to . '&key=JZKBZ-F5R6X-ZHC4W-T7TZC-5V3GV-NZBSX';

        $return = json_decode(cmf_curl_get($url), true);

        $field = explode(',', $field);

        if ($return['status'] == 0) {
            $res['distance'] = round($return['result']['routes'][0]['distance'] / 1000, 2);
            $res['duration'] = $return['result']['routes'][0]['duration'];
            $res['taxi_fare'] = $return['result']['routes'][0]['taxi_fare']['fare'];

            $data = [];
            foreach ($field as $k => $v) {
                if (!empty($res[$v])) {
                    $data[$v] = $res[$v];
                } else {
                    $data[$v] = 0;
                }
            }
            $run = json_encode($data);
        } else {
            $data = [];
            foreach ($field as $k => $v) {
                $data[$v] = 0;
            }
            $run = json_encode($data);
        }

        return $run;
    }

    /**
     * 评论列表
     * @OA\Get (
     *     tags={"陪玩小程序"},
     *     path="/wxapp/common/find_comment_list",
     * 	   @OA\Parameter(
     *         name="pw_id",
     *         in="query",
     *         description="陪玩ID",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function find_comment_list()
    {
        $pw_id = $this->request->param('pw_id');
        $Model = new CommentModel();
        $data = $Model->where(['pw_id' => $pw_id])->order('id desc')->paginate(10)->each(function ($item) {
            $user_info = MemberModel::where(['id' => $item['user_id']])->find();
            $item['nickname'] = $user_info['nickname'];
            $item['avatar'] = $user_info['avatar'];
            return $item;
        });

        $this->success('请求成功', $data);
    }

    /**
     * 门店信息
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\db\exception\DbException
     * @OA\Get (
     *     tags={"陪玩小程序"},
     *     path="/wxapp/common/find_shop",
     *     @OA\Parameter(
     *         name="cate_id",
     *         in="query",
     *         description="1吃饭 2观影 3酒馆 4网咖 5周边游",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="ID详情",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="lat",
     *         in="query",
     *         description="纬度",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="lng",
     *         in="query",
     *         description="经度",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function find_shop()
    {
        $id = $this->request->param('id');
        $cate_id = $this->request->param('cate_id', 1);
        $lat = $this->request->param('lat');
        $lng = $this->request->param('lng');

        if (empty($lat) || empty($lng)) $this->error('缺少位置信息');

        $Model = new ShopModel();
        if (!empty($id)) {
            $data = $Model->find($id);
            if (!empty($data)) {
                $data['logo'] = cmf_get_asset_url($data['logo']);

                $distance_info = json_decode($this->getDistance([$lat, $lng], [$data['lat'], $data['lng']]), true);
                $data['distance'] = $distance_info['distance'];

                $data['thumb'] = CommonModel::getArrImageFullUrl($data['thumb']);
                $data['address'] = $data['province'] . $data['city'] . $data['county'] . $data['address'];
            }
        } else {
            $map = [];

            $map[] = ['a.cate_id', '=', $cate_id];

            $data = $Model->alias('a')
                ->field("a.*,(6378.138 * 2 * asin(sqrt(pow(sin((a.lat * pi() / 180 - " . $lat . " * pi() / 180) / 2),2) + cos(a.lat * pi() / 180) * cos(" . $lat . " * pi() / 180) * pow(sin((a.lng * pi() / 180 - " . $lng . " * pi() / 180) / 2),2))) * 1000) as distance")
                ->where($map)->order('distance asc,id desc')
                ->paginate(10)->each(function ($item) use ($lat, $lng) {
                    $item['logo'] = cmf_get_asset_url($item['logo']);
                    $distance_info = json_decode($this->getDistance([$lat, $lng], [$item['lat'], $item['lng']]), true);
                    $item['distance'] = $distance_info['distance'];
                    $item['thumb'] = CommonModel::getArrImageFullUrl($item['thumb']);
                    return $item;
                });
        }
        $this->success('请求成功', $data);
    }

    /**
     * 获取陪玩路费
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\db\exception\DbException
     * @OA\Get (
     *     tags={"陪玩小程序"},
     *     path="/wxapp/common/get_pw_trip",
     *     @OA\Parameter(
     *         name="lat",
     *         in="query",
     *         description="纬度",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="lng",
     *         in="query",
     *         description="经度",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="pw_id",
     *         in="query",
     *         description="陪玩ID",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function get_pw_trip()
    {
        $lat = $this->request->param('lat');
        $lng = $this->request->param('lng');
        $pw_id = $this->request->param('pw_id');

//        if (empty($lat) || empty($lng)) $this->error('缺少位置信息');

        $play_with = PlayWithModel::where(['id' => $pw_id])->find();

        if (empty($play_with)) $this->error('陪玩信息错误');

        $distance_info = json_decode($this->getDistance([$lat, $lng], [$play_with['lat'], $play_with['lng']], 'distance,taxi_fare'), true);
        $distance = $distance_info['distance'];
       $pw_trip = $distance_info['taxi_fare'];
        //$distance = $this->getDistance([$play_with['lat'], $play_with['lng']], [$lat, $lng]);
        //$pw_trip = $this->getDistance([$play_with['lat'], $play_with['lng']], [$lat, $lng], 'taxi_fare');
        //$pw_trip = round($distance * cmf_get_option('set_config')['pw_trip'] * 2, 2);
        if (empty($pw_trip)) $pw_trip = round($distance * cmf_get_option('set_config')['pw_trip'] * 2, 2);

        $this->success('请求成功', $pw_trip);
    }

    /**
     * 创建订单
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\db\exception\DbException
     * @OA\Get (
     *     tags={"陪玩小程序"},
     *     path="/wxapp/common/add_order",
     *     @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="地址名称",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="address",
     *         in="query",
     *         description="地址",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="lat",
     *         in="query",
     *         description="纬度",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="lng",
     *         in="query",
     *         description="经度",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="pw_trip",
     *         in="query",
     *         description="路费",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="pw_id",
     *         in="query",
     *         description="陪玩ID",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="pay_type",
     *         in="query",
     *         description="支付方式 1微信 2余额",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="coupon_id",
     *         in="query",
     *         description="优惠券ID",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="remark",
     *         in="query",
     *         description="备注",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="time",
     *         in="query",
     *         description="小时量 默认一小时",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="cate_id",
     *         in="query",
     *         description="分类id",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function add_order()
    {
        $this->checkAuth();
        $data = $this->request->param();
        $lat = $this->request->param('lat');
        $lng = $this->request->param('lng');
        $pw_id = $this->request->param('pw_id');
        $pay_type = $this->request->param('pay_type', 1);
        $pw_trip = $this->request->param('pw_trip', 0);
        $time = $this->request->param('time', 1);
        $cate_id = $this->request->param('cate_id');

        if (empty($this->user_info['phone'])) $this->error('请完善您的手机号信息');

      //  if (empty($lat) || empty($lng) || empty($data['name']) || empty($data['address'])) $this->error('缺少位置信息');

        $play_with = PlayWithModel::where(['id' => $pw_id])->find();

        if (empty($play_with)) $this->error('陪玩信息错误');
        if ($play_with['status'] != 1) $this->error('陪玩正在忙碌');

        //$cate_info = CategoryModel::find($cate_id);
        //if (!empty($cate_info['time_limit']) && $cate_info['time_limit'] > $time) $this->error('该项服务最低' . $cate_info['time_limit'] . '小时起');

//        $distance = $this->getDistance([$play_with['lat'], $play_with['lng']], [$lat, $lng]);
//        $pw_trip = round($distance * cmf_get_option('set_config')['pw_trip'] * 2, 2);

        $price = $play_with['price'] * $time;

        $amount = $this->user_info['is_member'] == 1 ? ($price * 0.8) : $price;

        if (!empty($data['coupon_id'])) {
            $coupon = CouponModel::where(['id' => $data['coupon_id']])->find();
            if ($coupon['status'] != 1) $this->error('该优惠券已使用');
            if ($coupon['end_time'] < time()) $this->error('该优惠券已超时');
            $data['coupon_amount'] = $coupon['amount'];
        } else {
            $data['coupon_amount'] = 0;
        }
        $total_amount = $amount + $pw_trip - $data['coupon_amount'];

        $total_amount = round($total_amount, 2);

        $order_num = cmf_get_order_sn();

        $data['order_num'] = $order_num;
        $data['pw_lat'] = $play_with['lat'];
        $data['pw_lng'] = $play_with['lng'];
        $data['pw_price'] = $play_with['price'];
        $data['pw_trip'] = $pw_trip;
        $data['amount'] = $amount;
        $data['is_member'] = $this->user_info['is_member'];
        $data['total_amount'] = $total_amount;
        $data['user_id'] = $this->user_id;
        $data['cate_id'] = $cate_id;
        if ($pay_type == 2) {
            if ($this->user_info['balance'] < $data['total_amount']) $this->error('您的余额不足');
            $data['status'] = 2;
        }

        $Model = new OrderModel();
        $res = $Model->save($data);

        if ($res) {
            if ($pay_type == 1) {
                $Pay = new PayController();

                $pay_run = $Pay->wx_pay_mp($order_num, $data['total_amount'], $this->openid);

                if ($pay_run['code'] != 1) $this->error($pay_run['msg']);
                $this->success('请求成功', $pay_run['data']);
            } else {
                MemberModel::where(['id' => $this->user_id])->dec('balance', $data['total_amount'])->update();
                EarningsModel::insert(['user_id' => $this->user_id, 'type' => 3, 's_id' => $Model->id, 'price' => $data['total_amount'], 'create_time' => time()]);
                $this->newOrderSendSms($order_num);
                $this->success('支付成功');
            }
        } else {
            $this->error('请求失败');
        }
    }

    public function wx_pay_mp()
    {
        $Pay = new PayController();

        $openid = $this->request->param('openid');
        $order_num = cmf_get_order_sn();
        $res = $Pay->wx_pay_mp($order_num, 0.01, 'oWNQH6cSAV38Ifw3JrcAVtTrXmAg');

        if ($res['code'] != 1) $this->error($res['msg']);
        $this->success('请求成功', $res['data']);
    }

    public function updateOrder()
    {
        OrderModel::where('status', 'in', [2, 6])->where(['type' => 1])->select()->each(function ($item) {
            if (($item['create_time'] + 300) < time() && $item['status'] == 2) {
                if ($item['pay_type'] == 1) {
                    $Pay = new PayController();
                    $Pay->wx_pay_refund($item['order_num'], $item['total_amount']);
                } else {
                    MemberModel::where(['id' => $item['user_id']])->inc('balance', $item['total_amount'])->update();
                }
                OrderModel::where(['id' => $item['id']])->update(['status' => 10]);
            }
            if ($item['status'] == 6 && $item['expiration_time'] <= time()) {
                $append_order_amount = OrderAppendModel::where(['append_order_num' => $item['order_num'], 'status' => 2])->column('amount');
                PlayWithModel::where(['id' => $item['pw_id']])->update(['status' => 1]);
                $amount = $item['total_amount'];
                if (!empty($append_order_amount)) $amount = $amount + $append_order_amount;
                $user_id = PlayWithModel::where(['id' => $item['pw_id']])->value('user_id');
                MemberModel::where(['id' => $user_id])->inc('balance', $amount)->update();
                EarningsModel::insert(['user_id' => $user_id, 'type' => 2, 's_id' => $item['id'], 'price' => $amount, 'create_time' => time()]);
                OrderModel::where(['id' => $item['id']])->update(['status' => 8, 'completion_time' => time()]);
            }
        });
    }

    public function newOrderSendSms($order_num)
    {
        //if (empty($order_num)) $order_num = '20220707505797108948';
        $order_info = OrderModel::where(['order_num' => $order_num])->find();
        if (!empty($order_info)) {
            $user_phone = MemberModel::where(['id' => $order_info['user_id']])->value('phone');
            $pw_info = PlayWithModel::find($order_info['pw_id']);
            $sms = new \plugins\ali_sms\lib\Sms('LTAI5tD59HXxmjKARbTXRA4B', 'MlB9ubPhKf6paXqdMrpwCjzc8vhkXN');

            $phone = trim($pw_info['phone']);

            $params['PhoneNumbers'] = $phone;
            $params['SignName'] = '完美领域';
            $params['TemplateCode'] = 'SMS_246200405';
            $params['TemplateParam'] = array(
                'product' => $order_num,
                'number' => $user_phone
            );
            $res = $sms->sendSms($params);
            $this->success($sms->getError(), $res);
        } else {
            $this->error('发送失败');
        }
    }
}