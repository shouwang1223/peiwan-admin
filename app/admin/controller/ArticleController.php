<?php
namespace app\admin\controller;

use app\admin\model\ArticleModel;
use cmf\controller\AdminBaseController;
use Mpdf\Tag\A;

class ArticleController extends AdminBaseController
{
    public function list()
    {
        $model = new ArticleModel();
        $list = $model->order('id desc')->paginate(12);
        $page = $list->render();
        $this->assign('list', $list);
        $this->assign('page', $page);
        return $this->fetch();

    }
    public function add()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();
            $data['content'] = html_entity_decode($data['content']);
            $Model = new ArticleModel();

            $res = $Model->save($data);
            if ($res) {
                $this->success('添加成功',url('Article/list'));
            } else {
                $this->error('添加失败');
            }
        } else {
            return $this->fetch();
        }

    }

    public function edit()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();
            $data['content'] = html_entity_decode($data['content']);
            $Model = new ArticleModel();

            $info = $Model->find($data['id']);

            if (empty($info)) $this->error('不存在');

            $res = $Model->where(['id' => $data['id']])->save($data);
            if ($res) {
                $this->success('保存成功');
            } else {
                $this->error('保存失败');
            }
        } else {
            $shop_id = $this->request->param('id');
            $Model = new ArticleModel();
            $info = $Model->find($shop_id);
            $this->assign('result', $info);
            return $this->fetch();
        }
    }

    public function delete()
    {
        if ($this->request->isPost()) {
            $id = $this->request->param('id');
            $info = ArticleModel::find($id);
            if (empty($info)) {
                $this->error('不存在');
            } else {
                ArticleModel::destroy($id);
                $this->success("删除成功！");
            }
        } else {
            $this->error('非法操作');
        }
    }

}
