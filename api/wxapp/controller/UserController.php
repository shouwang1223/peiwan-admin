<?php

namespace api\wxapp\controller;

use app\admin\model\CategoryModel;
use app\admin\model\CommentModel;
use app\admin\model\CommonModel;
use app\admin\model\CouponModel;
use app\admin\model\EarningsModel;
use app\admin\model\MemberModel;
use app\admin\model\OrderAppendModel;
use app\admin\model\OrderModel;
use app\admin\model\PlayWithModel;
use app\admin\model\WithdrawModel;
use OpenApi\Annotations as OA;
use plugins\weipay\lib\PayController;
use think\Model;
use WeChat\Contracts\BasicWeChat;
use WeChat\Template;

class UserController extends AuthController
{

    /**
     * @OA\Post(
     *     tags={"陪玩小程序-个人中心"},
     *     summary="编辑个人信息",
     *     path="/wxapp/user/post_user_info",
     *     @OA\Parameter(
     *         name="avatar",
     *         in="query",
     *         description="avatar",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="nickname",
     *         in="query",
     *         description="nickname",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="area",
     *         in="query",
     *         description="地区",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function post_user_info()
    {
        $this->checkAuth();
        $data = $this->request->param();

        if (!empty($data['avatar'])) $data['avatar'] = cmf_get_asset_url($data['avatar']);
        if (empty($data['area'])) $this->error('请选择地区');

        MemberModel::where(['id' => $this->user_id])->update($data);

        $this->success('保存成功');
    }

    /**
     * 入驻陪玩
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\db\exception\DbException
     * @OA\Post(
     *     tags={"陪玩小程序-个人中心"},
     *     path="/wxapp/user/post_play_with",
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="传id 修改",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="nickname",
     *         in="query",
     *         description="昵称",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="birthday",
     *         in="query",
     *         description="生日",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="sex",
     *         in="query",
     *         description="性别",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="height",
     *         in="query",
     *         description="身高",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="weight",
     *         in="query",
     *         description="体重",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="professional",
     *         in="query",
     *         description="工作",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="thumb",
     *         in="query",
     *         description="照片",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="phone",
     *         in="query",
     *         description="手机号",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="lat",
     *         in="query",
     *         description="纬度",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="lng",
     *         in="query",
     *         description="经度",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function post_play_with()
    {
        $this->checkAuth();
        $data = $this->request->param();

        if (empty($data['cate_ids']) || empty($data['nickname']) || empty($data['birthday']) || empty($data['height']) || empty($data['weight']) || empty($data['professional']) || empty($data['thumb']) || empty($data['phone'])) $this->error('必填信息不能为空');

        $data['thumb'] = CommonModel::getStringImageUrl($data['thumb']);

        $data['cate_ids'] = json_encode($data['cate_ids']);

        $Model = new PlayWithModel();

        if (!empty($data['id'])) {
            unset($data['parent_id']);
            $res = $Model->where(['id' => $data['id']])->save($data);
            if ($res) {
                $this->success('保存成功！');
            } else {
                $this->error('保存失败');
            }
        } else {
            $find = $Model->where(['user_id' => $this->user_id])->find();

            if (empty($data['lat']) || empty($data['lng'])) $this->error('缺少位置信息');

            if (!empty($find)) $this->error('您已经提交过了');
            $data['parent_id'] = $this->user_info['parent_id'];
            $data['user_id'] = $this->user_id;
            $data['price'] = cmf_get_option('set_config')['pw_price'];
            $data['city'] = $this->search_address($data['lat'], $data['lng']);
            $res = $Model->save($data);
            if ($res) {
                $this->success('提交成功，请等待审核！');
            } else {
                $this->error('提交失败');
            }
        }
    }

    /**
     * 获取陪玩信息
     * @OA\Get (
     *     tags={"陪玩小程序-个人中心"},
     *     path="/wxapp/user/play_with_info",
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function play_with_info()
    {
        $this->checkAuth();
        $Model = new PlayWithModel();
        $data = $Model->find($this->user_info['play_with_id']);
        if (!empty($data)) {
            $data ['is_thumb'] = explode(',', $data['thumb']);
            $data['thumb'] = CommonModel::getArrImageFullUrl($data['thumb']);
            $thumb = [];
            if (!empty($data['thumb'])) {
                foreach ($data['thumb'] as $k => $v) {
                    $thumb[$k]['url'] = $v;
                }
            }
            $data['thumb'] = $thumb;
            $data['cate_ids'] = json_decode($data['cate_ids'], true);
        }
        $this->success('请求成功', $data);
    }

    /**
     * 我的优惠券
     * @OA\Get (
     *     tags={"陪玩小程序-个人中心"},
     *     path="/wxapp/user/find_coupon",
     * 	   @OA\Parameter(
     *         name="openid",
     *         in="query",
     *         description="openid",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function find_coupon()
    {
        $data = CouponModel::where(['user_id' => $this->user_id])->where('end_time', '>', time())->order('id desc')->paginate(10);
        $this->success('请求成功', $data);
    }

    /**
     * 订单列表
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\db\exception\DbException
     * @OA\Get (
     *     tags={"陪玩小程序-个人中心"},
     *     path="/wxapp/user/find_order_list",
     *     @OA\Parameter(
     *         name="status",
     *         in="query",
     *         description="2已支付 4已接单 6已到达 8已完成",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="type",
     *         in="query",
     *         description="1用户 2陪玩",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function find_order_list()
    {
        $this->checkAuth();
        $status = $this->request->param('status');
        $type = $this->request->param('type', 1);
        $map = [];
        if (!empty($status)) {
            if ($status == '4,6') {
                $map[] = ['a.status', 'in', [4, 6]];
            } else {
                $map[] = ['a.status', '=', $status];
            }
        } else {
            $map[] = ['a.status', '>=', 0];
        }
        if ($type == 2) {
            $map[] = ['b.user_id', '=', $this->user_id];
        } else {
            $map[] = ['a.user_id', '=', $this->user_id];
        }
        $map[] = ['a.type', '=', 1];
        $Model = new OrderModel();
        $data = $Model->alias('a')
            ->field('a.*,c.cate_name')
            ->join('pw_play_with b', 'a.pw_id = b.id')
            ->leftJoin('pw_category c', 'a.cate_id = c.id')
            ->where($map)->order('a.id desc')->paginate(10)->each(function ($item) {
                $comment = CommentModel::where(['order_id' => $item['id']])->find();
                if ($item['status'] == 8 && empty($comment)) {
                    $item['is_comment'] = 0;
                } else {
                    $item['is_comment'] = 1;
                }
                if ($item['user_id'] != $this->user_id) {
                    $user_info = MemberModel::where(['id' => $item['user_id']])->find();
                    if (!empty($user_info)) {
                        $item['nickname'] = $user_info['nickname'];
                        $item['phone'] = $user_info['phone'];
                        $item['avatar'] = $user_info['avatar'];
                    }
                } else {
                    $pw_info = PlayWithModel::where(['id' => $item['pw_id']])->find();
                    if (!empty($pw_info)) {
                        $item['nickname'] = $pw_info['nickname'];
                        $item['phone'] = $pw_info['phone'];
                        $item['avatar'] = CommonModel::getOneImageFullUrl($pw_info['thumb']);
                    }
                }

                if ($item['arrive_time'] + 300 > time()) {
                    $item['is_cancel'] = 0;
                } else {
                    $item['is_cancel'] = 1;
                }

                if ($item['create_time'] + 300 > time()) {
                    $item['cancel_time'] = ($item['create_time'] + 300) - time();
                } else {
                    $item['cancel_time'] = 0;
                }

                if ($item['expiration_time'] > time()) {
                    $item['expiration_time'] = $item['expiration_time'] - time();
                } else {
                    $item['expiration_time'] = 0;
                }

                $status_arr = [1 => '未支付', 2 => '已支付', 4 => '已接单', 6 => '已到达', 8 => '已完成', 10 => '已取消'];
                $item['is_status'] = $status_arr[$item['status']];
                return $item;
            });
        $this->success('请求成功', $data);
    }

    /**
     * 订单详情
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\db\exception\DbException
     * @OA\Get (
     *     tags={"陪玩小程序-个人中心"},
     *     path="/wxapp/user/find_order",
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="订单id",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="order_num",
     *         in="query",
     *         description="order_num",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function find_order()
    {
        $this->checkAuth();
        $id = $this->request->param('id');
        $order_num = $this->request->param('order_num');

        $map = [];
        if (!empty($id)) $map[] = ['id', '=', $id];
        if (!empty($order_num)) $map[] = ['order_num', '=', $order_num];

        $Model = new OrderModel();
        $data = $Model->where($map)->find();
        if (empty($data)) $this->error('订单不存在');

        $data['cate_name'] = CategoryModel::where('id', $data['cate_id'])->value('cate_name');

        $comment = CommentModel::where(['order_id' => $data['id']])->find();
        if ($data['status'] == 8 && empty($comment)) {
            $data['is_comment'] = 0;
        } else {
            $data['is_comment'] = 1;
        }

        if ($data['user_id'] != $this->user_id) {
            $user_info = MemberModel::where(['id' => $data['user_id']])->find();
            if (!empty($user_info)) {
                $data['nickname'] = $user_info['nickname'];
                $data['phone'] = $user_info['phone'];
                $data['avatar'] = $user_info['avatar'];
            }
        } else {
            $pw_info = PlayWithModel::where(['id' => $data['pw_id']])->find();
            if (!empty($pw_info)) {
                $data['nickname'] = $pw_info['nickname'];
                $data['phone'] = $pw_info['phone'];
                $data['avatar'] = CommonModel::getOneImageFullUrl($pw_info['thumb']);
            }
        }

        $status_arr = [1 => '未支付', 2 => '已支付', 4 => '已接单', 6 => '已到达', 8 => '已完成', 10 => '已取消'];
        $data['is_status'] = $status_arr[$data['status']];

        if ($data['arrive_time'] + 300 > time()) {
            $data['is_cancel'] = 0;
        } else {
            $data['is_cancel'] = 1;
        }

        if ($data['create_time'] + 300 > time()) {
            $data['cancel_time'] = ($data['create_time'] + 300) - time();
        } else {
            $data['cancel_time'] = 0;
        }

        if ($data['expiration_time'] > time()) {
            $data['expiration_time'] = $data['expiration_time'] - time();
        } else {
            $data['expiration_time'] = 0;
        }

        $this->success('请求成功', $data);
    }

    /**
     * 订单状态修改
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\db\exception\DbException
     * @OA\Get (
     *     tags={"陪玩小程序-个人中心"},
     *     path="/wxapp/user/update_order",
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="订单id",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="status",
     *         in="query",
     *         description="4接单 6已到达 8已完成 10取消",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function update_order()
    {
        $this->checkAuth();
        $id = $this->request->param('id');
        $status = $this->request->param('status');

        $Model = new OrderModel();
        $order_info = $Model->where(['id' => $id])->find();

        if (empty($order_info)) $this->error('订单不存在');

        if ($status == 10) {
            $Pay = new PayController();
            if ($order_info['status'] == 2) {
                if ($order_info['pay_type'] == 1) {
                    $res = $Pay->wx_pay_refund($order_info['order_num'], $order_info['total_amount']);
                    if ($res['code'] != 1) $this->error($res['msg']);
                } else {
                    MemberModel::where(['id' => $order_info['user_id']])->inc('balance', $order_info['total_amount'])->update();
                }
            } elseif ($order_info['status'] == 6 && time() < ($order_info['arrive_time'] + 300)) {
                if ($order_info['pay_type'] == 1) {
                    $res = $Pay->wx_pay_refund($order_info['order_num'], $order_info['amount'], $order_info['total_amount']);
                    if ($res['code'] != 1) $this->error($res['msg']);
                } else {
                    MemberModel::where(['id' => $order_info['user_id']])->inc('balance', $order_info['amount'])->update();
                }
            } else {
                $this->error('订单状态不支持取消');
            }
        } else {
            if ($order_info['status'] != ($status - 2)) $this->error('订单状态错误');
        }
        $data['status'] = $status;
        if ($status == 4) $data['order_time'] = time();
        if ($status == 6) {
            $data['arrive_time'] = time();
            $data['expiration_time'] = time() + (3600 * $order_info['time']);
        }
        if ($status == 8) $data['completion_time'] = time();

        $res = $Model->where('id', $id)->update($data);
        if ($res) {
            if ($status == 4) PlayWithModel::where(['id' => $order_info['pw_id']])->update(['status' => 2]);
            if ($status == 8) {
                $append_order_amount = OrderAppendModel::where(['append_order_num' => $order_info['order_num'], 'status' => 2])->column('amount');
                PlayWithModel::where(['id' => $order_info['pw_id']])->update(['status' => 1]);
                $amount = $order_info['total_amount'];
                if (!empty($append_order_amount)) $amount = $amount + $append_order_amount;
                $user_id = PlayWithModel::where(['id' => $order_info['pw_id']])->value('user_id');
                MemberModel::where(['id' => $user_id])->inc('balance', $amount)->update();
                EarningsModel::insert(['user_id' => $user_id, 'type' => 2, 's_id' => $id, 'price' => $amount, 'create_time' => time()]);
            }
            $this->success('操作成功');
        } else {
            $this->error('操作失败');
        }
    }

    /**
     * 订单追加时长
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\db\exception\DbException
     * @OA\Get (
     *     tags={"陪玩小程序-个人中心"},
     *     path="/wxapp/user/add_order_append",
     *     @OA\Parameter(
     *         name="append_order_num",
     *         in="query",
     *         description="追加订单号",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="append_time",
     *         in="query",
     *         description="追加时长",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function add_order_append()
    {
        $this->checkAuth();
        $data = $this->request->param();
        if (empty($data['append_order_num']) || empty($data['append_time'])) $this->error('请求错误，缺少参数');

        $Model = new OrderModel();
        $order_info = $Model->where(['order_num' => $data['append_order_num']])->find();

        if (empty($order_info)) $this->error('订单不存在');

        $pw_info = PlayWithModel::where(['id' => $order_info['pw_id']])->find();

        if (empty($pw_info)) $this->error('陪玩信息错误');

        $order_num = cmf_get_order_sn();

        $amount = $data['append_time'] * $pw_info['price'];
        $data['amount'] = $amount;
        $data['order_num'] = $order_num;
        $data['user_id'] = $this->user_id;

        $order['user_id'] = $this->user_id;
        $order['order_num'] = $order_num;
        $order['type'] = 2;
        $order['total_amount'] = $amount;
        $Model->save($order);

        $AppendModel = new OrderAppendModel();
        $res = $AppendModel->save($data);
        if ($res) {
            $Pay = new PayController();
            $pay_run = $Pay->wx_pay_mp($order_num, $amount, $this->openid);
            if ($pay_run['code'] != 1) $this->error($pay_run['msg']);
            $this->success('请求成功', $pay_run['data']);
        } else {
            $this->error('请求失败');
        }
    }

    /**
     * 订单评论
     * @OA\Get (
     *     tags={"陪玩小程序-个人中心"},
     *     path="/wxapp/user/add_comment",
     * 	   @OA\Parameter(
     *         name="order_id",
     *         in="query",
     *         description="订单ID",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     * 	   @OA\Parameter(
     *         name="content",
     *         in="query",
     *         description="评价",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function add_comment()
    {
        $this->checkAuth();
        $data = $this->request->param();

        $Model = new CommentModel();

        if (empty($data['order_id'])) $this->error('评论失败，请选择订单！');
        if (empty($data['content'])) $this->error('评论内容不能为空！');

        $info = $Model->where(['order_id' => $data['order_id']])->find();
        if (!empty($info)) $this->error('你已经评价过了');

        $order_info = OrderModel::where(['id' => $data['order_id']])->find();

        $data['pw_id'] = $order_info['pw_id'];
        $data['user_id'] = $this->user_id;
        $res = $Model->save($data);
        if ($res) {
            $this->success('评论成功');
        } else {
            $this->error('评论失败');
        }
    }

    /**
     * 购买会员订单
     * @OA\Get (
     *     tags={"陪玩小程序-个人中心"},
     *     path="/wxapp/user/add_member_order",
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function add_member_order()
    {
        $this->checkAuth();

        $Model = new OrderModel();

        $info = cmf_get_option('set_config');

        $order_num = cmf_get_order_sn();

        $order['order_num'] = $order_num;
        $order['type'] = 4;
        $order['total_amount'] = $info['member_price'];
        $order['user_id'] = $this->user_id;
        $res = $Model->save($order);
        if ($res) {
            $Pay = new PayController();
            $pay_run = $Pay->wx_pay_mp($order_num, $info['member_price'], $this->openid);
            if ($pay_run['code'] != 1) $this->error($pay_run['msg']);
            $this->success('请求成功', $pay_run['data']);
        } else {
            $this->error('请求失败');
        }
    }

    /**
     * 余额显示信息
     * @OA\Get (
     *     tags={"陪玩小程序-个人中心"},
     *     path="/wxapp/user/find_balance_info",
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function find_balance_info()
    {
        $data = [
            100,
            300,
            500,
            1000
        ];
        $this->success('请求成功', $data);
    }

    /**
     * 充值余额订单
     * @OA\Get (
     *     tags={"陪玩小程序-个人中心"},
     *     path="/wxapp/user/add_balance_order",
     *     @OA\Parameter(
     *         name="amount",
     *         in="query",
     *         description="金额",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function add_balance_order()
    {
        $this->checkAuth();
        $amount = $this->request->param('amount');

        $Model = new OrderModel();

        $order_num = cmf_get_order_sn();

        $order['order_num'] = $order_num;
        $order['type'] = 3;
        $order['total_amount'] = $amount;
        $order['user_id'] = $this->user_id;
        $res = $Model->save($order);
        if ($res) {
            $Pay = new PayController();
            $pay_run = $Pay->wx_pay_mp($order_num, $amount, $this->openid);
            if ($pay_run['code'] != 1) $this->error($pay_run['msg']);
            $this->success('请求成功', $pay_run['data']);
        } else {
            $this->error('请求失败');
        }
    }

    /**
     * 我的下级
     * @OA\Get(
     *     tags={"陪玩小程序-个人中心"},
     *     path="/wxapp/user/my_child",
     *     @OA\Parameter(
     *         name="openid",
     *         in="query",
     *         description="openid",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *    @OA\Response(response="default", description="An example resource")
     * )
     */
    public function my_child()
    {
        $this->checkAuth();
        $Model = new MemberModel();
        $map = [];
        $map['parent_id'] = $this->user_id;

        $data = $Model->field('id user_id,nickname,avatar,create_time')->where($map)->order('id desc')->paginate(10);

        $this->success('获取成功', $data);
    }

    /**
     * 提现余额
     * @OA\Get (
     *     tags={"陪玩小程序-个人中心"},
     *     path="/wxapp/user/add_withdraw",
     *     @OA\Parameter(
     *         name="amount",
     *         in="query",
     *         description="金额",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="zfb_num",
     *         in="query",
     *         description="支付宝账号",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function add_withdraw()
    {
        $this->checkAuth();
        $data = $this->request->param();
        if (empty($data['amount'])) $this->error('请填金额');
        if ($data['amount'] > $this->user_info['balance']) $this->error('提现金额不能大于总余额');

        if (empty($data['zfb_num'])) $this->error('请填写支付宝账号');

        $Model = new WithdrawModel();

        $data['user_id'] = $this->user_id;
        $res = $Model->save($data);
        if ($res) {
            MemberModel::where(['id' => $this->user_id])->dec('balance', $data['amount'])->update();
            $this->success('提交成功，请等待审核！');
        } else {
            $this->error('提交失败');
        }
    }

    /**
     * 余额明细
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\db\exception\DbException
     * @OA\Post(
     *     tags={"陪玩小程序-个人中心"},
     *     path="/wxapp/user/find_balance_detail",
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function find_balance_detail()
    {
        $this->checkAuth();
        $Model = new EarningsModel();
        $data = $Model->where(['user_id' => $this->user_id])->order('id desc')->paginate(10)->each(function ($item) {
            $type_arr = [1 => '充值', 2 => '订单收益', 3 => '下单陪玩'];
            $price_arr = [1 => '+', 2 => '+', 3 => '-'];
            $item['is_type'] = $type_arr[$item['type']];
            $item['price'] = $price_arr[$item['type']] . $item['price'];
            return $item;
        });
        $this->success('请求成功', $data);
    }

    /**
     * 提现明细
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\db\exception\DbException
     * @OA\Post(
     *     tags={"陪玩小程序-个人中心"},
     *     path="/wxapp/user/find_withdraw_detail",
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function find_withdraw_detail()
    {
        $this->checkAuth();

        $Model = new WithdrawModel();

        $data = $Model->where('user_id', $this->user_id)->paginate(10)->each(function ($item) {
            $status_arr = [1 => '未审核', 2 => '已发放', 3 => '已驳回(金额已退回余额)'];
            $item['is_status'] = $status_arr[$item['status']];
            return $item;
        });

        $this->success('请求成功', $data);
    }

    public function sendSubscribeMsg($order_num)
    {
        $order_info = OrderModel::where(['order_num' => $order_num])->find();

        $openid = MemberModel::where('id', $order_info['user_id'])->value('openid');

        $template_id = "yLpTpePhBzVBI7Lbm-ncByppwX9gFNMWa5hOfCxXyKM";

        $data = [];
        //接收者（用户）的 openid
        $data['touser'] = $openid;
        //所需下发的订阅模板id
        $data['template_id'] = $template_id;
        //点击模板卡片后的跳转页面，仅限本小程序内的页面。支持带参数,（示例index?foo=bar）。该字段不填则模板无跳转。
        $data['page'] = "pages/index/index";

        $data['data'] = [
            "character_string1" => ['value' => $order_num],
            "thing2" => ['value' => '陪玩已接单'],
            "thing3" => ['value' => '请电话保持畅通，方便您的陪玩联系您。'],
        ];

        //跳转小程序类型：developer为开发版；trial为体验版；formal为正式版；默认为正式版
        //$data['miniprogram_state'] = 'trial';

        try {
            $plugin_config = cmf_get_option('weipay');

            $WeMini = new Template([
                //微信基本信息
                'token' => $plugin_config['wx_token'],
                'appid' => !empty($plugin_config['wx_mp_app_id']) ? $plugin_config['wx_mp_app_id'] : $plugin_config['wx_mini_app_id'],
                'appsecret' => !empty($plugin_config['wx_mp_app_secret']) ? $plugin_config['wx_mp_app_secret'] : $plugin_config['wx_mini_app_secret'],
                'encodingaeskey' => $plugin_config['wx_encodingaeskey'],
                // 配置商户支付参数
                'mch_id' => $plugin_config['wx_mch_id'],
                'mch_key' => $plugin_config['wx_v2_mch_secret_key'],
                // 配置商户支付双向证书目录 （p12 | key,cert 二选一，两者都配置时p12优先）
                //	'ssl_p12'        => __DIR__ . DIRECTORY_SEPARATOR . 'cert' . DIRECTORY_SEPARATOR . '1332187001_20181030_cert.p12',
                'ssl_key' => './upload/' . $plugin_config['wx_mch_secret_cert'],
                'ssl_cer' => './upload/' . $plugin_config['wx_mch_public_cert_path'],
                // 配置缓存目录，需要拥有写权限
                'cache_path' => './wx_cache_path',
            ]);
            $WeMini->send($data);
        } catch (\Exception $exception) {
            echo $exception->getMessage();
        }
    }
}