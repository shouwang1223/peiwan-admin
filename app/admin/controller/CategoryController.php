<?php


namespace app\admin\controller;

use app\admin\model\CategoryModel;
use cmf\controller\AdminBaseController;

class CategoryController extends AdminBaseController
{
    /**
     * 显示资源列表
     */
    public function index()
    {
        $keyword = $this->request->param('keyword');

        $map = [];
        if (!empty($keyword)) $map[] = ['cate_name', 'like', "%$keyword%"];

        $Model = new CategoryModel();
        $list = $Model->where($map)->order(['list_order' => 'ASC'])->paginate(12);
        $list->appends(['keyword' => $keyword]);
        $page = $list->render();
        $this->assign('page', $page);
        $this->assign('list', $list);
        return $this->fetch();
    }

    /**
     * 保存新建的资源
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();

            $Model = new CategoryModel();
            $res = $Model->save($data);
            if ($res) {
                $this->success('添加成功', url('Category/index'));
            } else {
                $this->error('添加失败');
            }
        } else {
            return $this->fetch();
        }
    }

    public function listOrder()
    {
        $linkModel = new CategoryModel();
        parent::listOrders($linkModel);
        $this->success("排序更新成功！");
    }

    /**
     * 显示编辑资源表单页.
     */
    public function edit()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();

            $Model = new CategoryModel();

            $info = $Model->find($data['id']);

            if (empty($info)) $this->error('不存在');

            $res = $Model->where(['id' => $data['id']])->save($data);
            if ($res) {
                $this->success('修改成功', url('Category/index'));
            } else {
                $this->error('修改失败');
            }
        } else {
            $id = $this->request->param('id');
            $Model = new CategoryModel();
            $info = $Model->find($id);
            $this->assign('info', $info);
            return $this->fetch();
        }
    }

    /**
     * 删除指定资源
     */
    public function delete()
    {
        if ($this->request->isPost()) {
            $id = $this->request->param('id');
            $Model = new CategoryModel();
            $info = $Model->find($id);
            if (empty($info)) {
                $this->error('不存在');
            } else {
                $Model->destroy($id);
                $this->success("删除成功！");
            }
        } else {
            $this->error('非法操作');
        }
    }
}