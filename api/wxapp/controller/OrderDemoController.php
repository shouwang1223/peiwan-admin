<?php

namespace api\wxapp\controller;

use cmf\controller\RestBaseController;
use plugins\weipay\lib\PayController;

class OrderDemoController extends RestBaseController
{
    /**
     * 微信小程序支付
     * @OA\Get(
     *     tags={"订单支付Demo"},
     *     path="/wxapp/order_demo/wx_pay_mini",
     * 	   @OA\Parameter(
     *         name="openid",
     *         in="query",
     *         description="openid",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function wx_pay_mini()
    {
        $Pay = new PayController();
        $openid = $this->request->param('openid');
        $order_num = cmf_get_order_sn();
        $res = $Pay->wx_pay_mini($order_num, 0.01, $openid);
        if ($res['code'] != 1) $this->error($res['msg']);
        $this->success('请求成功',$res['data']);
    }

    /**
     * 微信公众号支付
     * @OA\Get(
     *     tags={"订单支付Demo"},
     *     path="/wxapp/order_demo/wx_pay_mp",
     * 	   @OA\Parameter(
     *         name="openid",
     *         in="query",
     *         description="openid",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function wx_pay_mp()
    {
        $Pay = new PayController();
        $openid = $this->request->param('openid');
        $order_num = cmf_get_order_sn();
        $res = $Pay->wx_pay_mp($order_num, 0.01, $openid);
        if ($res['code'] != 1) $this->error($res['msg']);
        $this->success('请求成功',$res['data']);
    }

    /**
     * 微信App支付
     * @OA\Get(
     *     tags={"订单支付Demo"},
     *     path="/wxapp/order_demo/wx_pay_app",
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function wx_pay_app()
    {
        $Pay = new PayController();
        $order_num = cmf_get_order_sn();
        $res = $Pay->wx_pay_app($order_num, 0.01);
        if ($res['code'] != 1) $this->error($res['msg']);
        $this->success('请求成功',$res['data']);
    }

    /**
     * 微信订单退款
     * @OA\Get(
     *     tags={"订单支付Demo"},
     *     path="/wxapp/order_demo/wx_pay_refund",
     * 	   @OA\Parameter(
     *         name="order_num",
     *         in="query",
     *         description="order_num",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     * 	   @OA\Parameter(
     *         name="amount",
     *         in="query",
     *         description="amount",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function wx_pay_refund()
    {
        $Pay = new PayController();
        $order_num = $this->request->param('order_num');
        $amount = $this->request->param('amount');
        $res = $Pay->wx_pay_refund($order_num, $amount);
        if ($res['code'] != 1) $this->error($res['msg']);
        $this->success('请求成功',$res['data']);
    }

    /**
     * 支付宝APP支付
     * @OA\Get(
     *     tags={"订单支付Demo"},
     *     path="/wxapp/order_demo/ali_pay_app",
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function ali_pay_app()
    {
        $Pay = new PayController();
        $order_num = cmf_get_order_sn();
        $res = $Pay->ali_pay_app($order_num, 0.01);
        if ($res['code'] != 1) $this->error($res['msg']);
        $this->success('请求成功',$res['data']);
    }

    /**
     * 支付宝订单退款
     * @OA\Get(
     *     tags={"订单支付Demo"},
     *     path="/wxapp/order_demo/ali_pay_refund",
     * 	   @OA\Parameter(
     *         name="order_num",
     *         in="query",
     *         description="order_num",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     * 	   @OA\Parameter(
     *         name="amount",
     *         in="query",
     *         description="amount",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function ali_pay_refund()
    {
        $Pay = new PayController();
        $order_num = $this->request->param('order_num');
        $amount = $this->request->param('amount');
        $res = $Pay->ali_pay_refund($order_num, $amount);
        if ($res['code'] != 1) $this->error($res['msg']);
        $this->success('请求成功',$res['data']);
    }
}