<?php
/**
 * Created by PhpStorm.
 * User: zhang
 * Date: 2021/9/25
 * Time: 11:31
 */

namespace app\admin\controller;

use app\admin\model\CommentModel;
use app\admin\model\CommonModel;
use app\admin\model\MemberModel;
use app\admin\model\OrderModel;
use app\admin\model\PlayWithModel;
use app\admin\model\RoleUserModel;
use app\admin\model\UserModel;
use cmf\controller\AdminBaseController;
use think\Model;

class PlayWithController extends AdminBaseController
{
    /**
     * 显示资源列表
     */
    public function index()
    {
        $keyword = $this->request->param('keyword');

        $map = [];
        if (!empty($keyword)) $map[] = ['id|nickname|phone|city|birthday|professional', 'like', "%$keyword%"];
        $role_id = RoleUserModel::where(['user_id' => cmf_get_current_admin_id(), 'role_id' => 2])->find();
        if (!empty($role_id)) {
            $parent_id = UserModel::where(['id' => cmf_get_current_admin_id()])->value('parent_id');
            $map[] = ['parent_id', '=', $parent_id];
        }

        $model = new PlayWithModel();
        $list = $model->where($map)->order('id desc')->paginate(12)->each(function ($item) {
            $status_arr = [0 => '未审核', 1 => '在线', 2 => '忙碌'];
            $item['is_status'] = $status_arr[$item['status']];
            $item['image'] = CommonModel::getOneImageFullUrl($item['thumb']);

            $item['parent_name'] = MemberModel::where(['id' => $item['parent_id']])->value('nickname');

            $order_list = OrderModel::where(['pw_id' => $item['id']])->where('status', 'in', [6, 8])->select()->each(function ($item) {
                $item['total_time'] = $item['expiration_time'] - $item['arrive_time'];
                return $item;
            })->toArray();
            $total_time = array_sum(array_column($order_list, 'total_time'));
            $item['total_time'] = $total_time / 60;
            return $item;
        });

        $list->appends(['keyword' => $keyword]);
        $page = $list->render();
        $this->assign('page', $page);
        $this->assign('list', $list);
        return $this->fetch();
    }

    /**
     * 显示编辑资源表单页.
     */
    public function update()
    {
        if ($this->request->isPost()) {
            $id = $this->request->param('id');

            $Model = new PlayWithModel();

            $info = $Model->find($id);

            if (empty($info)) $this->error('不存在');

            if ($info['status'] != 0) $this->error('该陪玩已通过');

            $res = $Model->where(['id' => $id])->save(['status' => 1]);
            if ($res) {
                $this->success('操作成功');
            } else {
                $this->error('操作失败');
            }
        } else {
            $this->error('非法操作');
        }
    }

    /**
     * 显示编辑资源表单页.
     */
    public function is_top()
    {
        if ($this->request->isPost()) {
            $id = $this->request->param('id');

            $Model = new PlayWithModel();

            $info = $Model->find($id);

            if (empty($info)) $this->error('不存在');

            $is_top = $info['is_top'] == 1 ? 0 : 1;

            $res = $Model->where(['id' => $id])->save(['is_top' => $is_top]);
            if ($res) {
                $this->success('操作成功');
            } else {
                $this->error('操作失败');
            }
        }
    }

    /**
     * 显示编辑资源表单页.
     */
    public function edit()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();

            $Model = new PlayWithModel();

            $info = $Model->find($data['id']);

            if (empty($info)) $this->error('不存在');

            if (empty($data['thumb'])) $this->error('请上传轮播图');

            $data['thumb'] = implode(',', $data['thumb']);

            $res = $Model->where(['id' => $data['id']])->save($data);
            if ($res) {
                $this->success('保存成功');
            } else {
                $this->error('保存失败');
            }
        } else {
            $id = $this->request->param('id');

            $Model = new PlayWithModel();

            $info = $Model->find($id);

            $info['thumb'] = explode(',', $info['thumb']);

            $this->assign('info', $info);
            return $this->fetch();
        }
    }


    public function comment()
    {
        $parent_id = $this->request->param('parent_id');
        $keyword = $this->request->param('keyword');

        $map = [];
        if (!empty($keyword)) $map[] = ['b.nickname|a.content', 'like', "%$keyword%"];
        $map[] = ['a.pw_id', '=', $parent_id];

        $model = new CommentModel();
        $list = $model->alias('a')
            ->field('a.*,b.nickname')
            ->join('member b', 'a.user_id = b.id')
            ->where($map)
            ->order('a.id desc')->paginate(12);

        $list->appends(['keyword' => $keyword, 'parent_id' => $parent_id]);
        $page = $list->render();
        $this->assign('page', $page);
        $this->assign('list', $list);
        return $this->fetch();
    }

    /**
     * 删除指定资源
     */
    public function comment_delete()
    {
        if ($this->request->isPost()) {
            $id = $this->request->param('id');
            $info = CommentModel::find($id);
            if (empty($info)) {
                $this->error('不存在');
            } else {
                CommentModel::destroy($id);
                $this->success("删除成功！");
            }
        } else {
            $this->error('非法操作');
        }
    }

    /**
     * 删除指定资源
     */
    public function delete()
    {
        if ($this->request->isPost()) {
            $id = $this->request->param('id');
            $info = PlayWithModel::find($id);
            if (empty($info)) {
                $this->error('不存在');
            } else {
                PlayWithModel::destroy($id);
                $this->success("删除成功！");
            }
        } else {
            $this->error('非法操作');
        }
    }
}