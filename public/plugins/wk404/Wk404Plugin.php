<?php
/**
 * Created by birdtool.com
 * Author: birdtool.com
 * webSite: www.birdtool.com
 * Date: 2019/11/7
 * Time: 下午4:25
 */

namespace plugins\wk404;
use cmf\lib\Plugin;

class Wk404Plugin extends Plugin
{

    public $info = [
        'name'        => 'wk404',
        'title'       => '悟空404页面',
        'description' => '悟空404页面',
        'status'      => 1,
        'author'      => 'thinkcmf.com',
        'version'     => '1.0.0',
        'demo_url'    => 'https://www.thinkcmf.com',
        'author_url'  => 'https://www.thinkcmf.com'
    ];

    public $hasAdmin = 0;

    public function install()
    {
        // 动态修改异常页面路径
        cmf_set_dynamic_config([
            'app' => [
                'exception_tmpl' => CMF_ROOT . 'public/plugins/wk404/404.html' // 404页面可自定义修改
            ]
        ]);
        return true;
    }

    public function uninstall()
    {
        // 动态修改异常页面路径
        cmf_set_dynamic_config([
            'app' => [
                'exception_tmpl' => CMF_ROOT . 'vendor/thinkphp/tpl/think_exception.tpl'
            ]
        ]);
        return true;
    }
}