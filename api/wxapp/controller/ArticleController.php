<?php

namespace api\wxapp\controller;

use cmf\controller\RestBaseController;
use think\facade\Db;

class ArticleController extends RestBaseController
{
    /**
     * 详情
     * @OA\Get (
     *     tags={"陪玩小程序"},
     *     path="/wxapp/article/detail",
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function detail()
    {
        $id = input('id/d', 0);
        $row = Db::name('article')->where('id', $id)->find();
        if (!$row) {
            $this->error('信息不存在');
        }
        $this->success('请求成功', $row);
    }
}