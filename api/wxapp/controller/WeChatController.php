<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-present http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\wxapp\controller;

use app\admin\model\CommonModel;
use think\facade\Db;
use think\facade\Log;

class WeChatController extends AuthController
{
	public function receive()
	{
        $plugin_config = cmf_get_option('weipay');

        $config = [
            //微信基本信息
            'token' => $plugin_config['wx_token'],
            'appid' => !empty($plugin_config['wx_mp_app_id']) ? $plugin_config['wx_mp_app_id'] : $plugin_config['wx_mini_app_id'],
            'appsecret' => !empty($plugin_config['wx_mp_app_secret']) ? $plugin_config['wx_mp_app_secret'] : $plugin_config['wx_mini_app_secret'],
            'encodingaeskey' => $plugin_config['wx_encodingaeskey'],
            // 配置商户支付参数
            'mch_id' => $plugin_config['wx_mch_id'],
            'mch_key' => $plugin_config['wx_v2_mch_secret_key'],
            // 配置商户支付双向证书目录 （p12 | key,cert 二选一，两者都配置时p12优先）
            //	'ssl_p12'        => __DIR__ . DIRECTORY_SEPARATOR . 'cert' . DIRECTORY_SEPARATOR . '1332187001_20181030_cert.p12',
            'ssl_key' => './upload/' . $plugin_config['wx_mch_secret_cert'],
            'ssl_cer' => './upload/' . $plugin_config['wx_mch_public_cert_path'],
            // 配置缓存目录，需要拥有写权限
            'cache_path' => './wx_cache_path',
        ];
        
		$WeChat = new \WeChat\Contracts\BasicPushEvent($config);

		if ($WeChat->getMsgType() === 'text') {

			$openid = $WeChat->getOpenid();
			$to_openid = $WeChat->getToOpenid();

			//$user_info = $this->getUserInfoByOpenid($openid);

            //返回信息
            //$data = [
            //    'ToUserName' => $openid,
            //    'FromUserName' => $to_openid,
            //    'CreateTime' => time(),
            //    'MsgType' => 'text',
            //    'Content' => '',
            //];
            //try {
            //    ob_clean();
            //    $send = $WeChat->reply($data, true);
            //    echo $send;
            //    exit;
            //} catch (\Exception $e) {
            //    // 出错啦，处理下吧
            //    echo $e->getMessage() . PHP_EOL;
            //}
        } else {
            @ob_clean();
            exit($this->request->get('echostr'));
        }
	}
}
