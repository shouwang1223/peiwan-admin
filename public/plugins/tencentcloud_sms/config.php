<?php

return array(
    'accessKeyId' => array(
        'title' => '应用accessKeyId',
        'type' => 'text',
        'value' => '',
        'tip' => ''
    ),
    'accessKeySecret' => array(
        'title' => '密钥accessKeySecret',
        'type' => 'text',
        'value' => '',
        'tip' => ''
    ),
    'SignName' => array(
        'title' => '短信签名',
        'type' => 'text',
        'value' => '',
        'tip' => ''
    ),
    'TemplateID'=>[
        'title' => '短信模板ID',
        'type' => 'text',
        'value' => '',
        'tip' => ''
    ],
    'AppIDSdk'=>[
        'title' => '短信应用ID',
        'type' => 'text',
        'value' => '',
        'tip' => ''
    ]
);
