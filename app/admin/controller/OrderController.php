<?php
/**
 * Created by PhpStorm.
 * User: zhang
 * Date: 2021/9/25
 * Time: 11:31
 */

namespace app\admin\controller;

use app\admin\model\CategoryModel;
use app\admin\model\CommonModel;
use app\admin\model\MemberModel;
use app\admin\model\PlayWithModel;
use app\admin\model\RoleUserModel;
use app\admin\model\UserModel;
use cmf\controller\AdminBaseController;
use app\admin\model\OrderModel;

class OrderController extends AdminBaseController
{
    /**
     * 显示资源列表
     * @throws \think\db\exception\DbException
     */
    public function index()
    {
        $keyword = $this->request->param('keyword');
        $status = $this->request->param('status');

        $map = [];
        if (!empty($keyword)) $map[] = ['id|name|address|order_num', 'like', "%$keyword%"];
        if (!empty($status)) {
            $map[] = ['status', '=', $status];
        } else {
            $map[] = ['status', '>', 1];
        }
        $map[] = ['type', '=', 1];

        $role_id = RoleUserModel::where(['user_id' => cmf_get_current_admin_id(), 'role_id' => 2])->find();
        if (!empty($role_id)) {
            $parent_id = UserModel::where(['id' => cmf_get_current_admin_id()])->value('parent_id');
            $pw_ids = PlayWithModel::where(['parent_id' => $parent_id])->column('id');
            $map[] = ['pw_id', 'in', $pw_ids];
        }

        $model = new OrderModel();
        $list = $model->where($map)->order('id desc')->paginate(12)->each(function ($item) {
            $item['nickname'] = MemberModel::where(['id' => $item['user_id']])->value('nickname');
            $item['cate_name'] = CategoryModel::where(['id' => $item['cate_id']])->value('cate_name');
            $item['pw_nickname'] = PlayWithModel::where(['id' => $item['pw_id']])->value('nickname');
            $status_arr = [2 => '待接单', 4 => '已接单', 6 => '已到达', 8 => '已完成', 10 => '已取消'];
            $item['is_status'] = $status_arr[$item['status']];
            return $item;
        });

        $list->appends(['keyword' => $keyword, 'status' => $status]);
        $page = $list->render();

        $status_arr = [2 => '待接单', 4 => '已接单', 6 => '已到达', 8 => '已完成', 10 => '已取消'];
        $this->assign('status_arr', $status_arr);
        $this->assign('page', $page);
        $this->assign('list', $list);
        return $this->fetch();
    }

    public function order_info()
    {
        $id = $this->request->param('id');
        $order_num = $this->request->param('order_num');
        if (!empty($order_num)) {
            $info = OrderModel::where(['order_num' => $order_num])->find();
        } else {
            $info = OrderModel::find($id);
        }

        $status_arr = [2 => '待接单', 4 => '已接单', 6 => '已到达', 8 => '已完成', 10 => '已取消'];
        $info['is_status'] = $status_arr[$info['status']];
        $info['cate_name'] = CategoryModel::where(['id' => $info['cate_id']])->value('cate_name');

        $info['user_info'] = MemberModel::where(['id' => $info['user_id']])->find();
        if (!empty($info['user_info'])) $info['user_info']['avatar'] = cmf_get_asset_url($info['user_info']['avatar']);

        $info['pw_info'] = PlayWithModel::where(['id' => $info['pw_id']])->find();
        if (!empty($info['pw_info'])) $info['pw_info']['image'] = CommonModel::getOneImageFullUrl($info['pw_info']['thumb']);

        $this->assign('info', $info);
        return $this->fetch();
    }

    /**
     * 删除指定资源
     */
    public function delete()
    {
        if ($this->request->isPost()) {
            $id = $this->request->param('id');
            $info = OrderModel::find($id);
            if (empty($info)) {
                $this->error('不存在');
            } else {
                OrderModel::destroy($id);
                $this->success("删除成功！");
            }
        } else {
            $this->error('非法操作');
        }
    }
}