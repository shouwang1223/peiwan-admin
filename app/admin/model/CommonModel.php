<?php
/**
 * Created by PhpStorm.
 * User: zhang
 * Date: 2021/9/25
 * Time: 16:25
 */

namespace app\admin\model;

use cmf\lib\Storage;
use think\model;

class CommonModel extends model
{
    /**
     * 模型名称
     * @var string
     */
    protected $name = '';

    /*
    * 获取字符串的图片
    */
    static public function getStringImageUrl($image_url)
    {
        if (!empty($image_url)) {
            if (is_array($image_url)) {
                $result = implode(',', $image_url);
            } else {
                $that_image_url = $image_url;
                $decode_image_url = json_decode($image_url, true);
                if (empty($decode_image_url)) {
                    $result = $that_image_url;
                } else {
                    if (is_array($decode_image_url)) {
                        $result = implode(',', $decode_image_url);
                    } else {
                        $result = $decode_image_url;
                    }
                }
            }
        } else {
            $result = '';
        }
        return $result;
    }

    /*
    * 获取数组形式的的全路径图片
    */
    static public function getArrImageFullUrl($image_url): array
    {
        $show_image_url = [];
        if (!empty($image_url)) {
            $image_url_arr = explode(',', $image_url);
            foreach ($image_url_arr as $key => $value) {
                $show_image_url[] = cmf_get_asset_url($value);
            }
        }

        return $show_image_url;
    }

    /*
    * 获取一个的的全路径图片
    */
    static public function getOneImageFullUrl($image_url)
    {
        $show_image_url = '';
        if (!empty($image_url)) {
            $image_url_arr = explode(',', $image_url);
            $image_arr = [];
            foreach ($image_url_arr as $key => $value) {
                $image_arr[] = cmf_get_asset_url($value);
            }
            $show_image_url = $image_arr[0];
        }
        return $show_image_url;
    }

    /*
    * 获取编辑时的全路径和半路径
    * $num 1取一维
    */
    static function getEditImageUrl($image_path, $num = 0): array
    {
        $image_url_arr = [];
        if (!empty($image_path)) {
            if ($num == 1) {
                $image_url_arr['show_image'] = cmf_get_image_url($image_path);
                $image_url_arr['image'] = $image_path;
            } else {
                $image_url = explode(',', $image_path);

                foreach ($image_url as $key => $value) {
                    $image_url_arr[$key]['show_image'] = cmf_get_image_url($value);
                    $image_url_arr[$key]['image'] = $value;
                }
            }
        }
        return $image_url_arr;
    }

    /*
    * 生成随机字符串
    */
    static function randomKeys($length): string
    {
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLOMNOPQRSTUVWXYZ';
        $key = '';
        for ($i = 0; $i < $length; $i++) {
            $key .= $pattern{mt_rand(0, 35)};    //生成php随机数
        }
        return $key;
    }

    /*
    * 生成唯一订单号
    */
    static function createOrderNumber(): string
    {
        return date('Ymd') . substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
    }

    static function getCodeQrImg($data): string
    {
        require_once '../vendor/phpqrcode/phpqrcode.php';

        $dir = './upload/qr_code/' . date('Ymd');

        if (!file_exists($dir)) {
            mkdir($dir);
        }

        $filename = time() . rand(1000, 9999);

        $outfile = $dir . '/' . $filename . '.png';
        $QRcode = new \QRcode();

        //生成png图片
        $QRcode->png($data, $outfile, 'L', 10, 1, false, 0xFFFFFF, 0x000000);

        $plugin_config = cmf_get_plugin_config('Oss');
        $file_path = $plugin_config['dir'] . '/qr_code/' . date('Ymd') . '/' . $filename . '.png';

        $upload_path = $dir . '/' . $filename . '.png';

        $storage = cmf_get_option('storage');
        $storage = new Storage($storage['type'], $storage['storages'][$storage['type']]);
        $storage->upload($file_path, $upload_path);

        return $file_path;
    }
}