<?php
/**
 * Created by PhpStorm.
 * User: zhang
 * Date: 2021/9/25
 * Time: 11:31
 */

namespace app\admin\controller;

use app\admin\model\RoleUserModel;
use app\admin\model\UserModel;
use cmf\controller\AdminBaseController;
use app\admin\model\MemberModel;

class MemberController extends AdminBaseController
{
    public function index()
    {
        $keyword = $this->request->param('keyword');
        $map = [];
        if (!empty($keyword)) $map[] = ['id|nickname', 'like', "%$keyword%"];
        $model = new MemberModel();
        $list = $model->where($map)->order('id desc')->paginate(12)->each(function ($item) {
            $item['parent_name'] = MemberModel::where(['id' => $item['parent_id']])->value('nickname');
            return $item;
        });
        $list->appends(['keyword' => $keyword]);
        $page = $list->render();
        $this->assign('page', $page);
        $this->assign('list', $list);
        return $this->fetch();
    }

    /**
     * 删除指定资源
     */
    public function delete()
    {
        if ($this->request->isPost()) {
            $ids = $this->request->param('ids');

            MemberModel::destroy($ids);
            $this->success("删除成功！");
        } else {
            $this->error('非法操作');
        }
    }

    public function update()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();

            $user_info = UserModel::where(['parent_id' => $data['parent_id']])->find();

            if (!empty($data['user_pass'])) {
                $data['user_pass'] = cmf_password($data['user_pass']);
            } else {
                unset($data['user_pass']);
            }

            if (!empty($user_info)) {
                UserModel::where(['id' => $user_info['id']])->update($data);
            } else {
                if (empty($data['user_pass'])) $this->error('请填写密码');
                $data['create_time'] = time();
                $user_id = UserModel::insertGetId($data);
                RoleUserModel::insert(['user_id' => $user_id, 'role_id' => 2]);
                MemberModel::where(['id' => $data['parent_id']])->update(['type' => 2]);
            }
            $this->success('保存成功');
        } else {
            $parent_id = $this->request->param('parent_id');
            $user_info = UserModel::where(['parent_id' => $parent_id])->find();
            $this->assign('parent_id', $parent_id);
            $this->assign('user_info', $user_info);
            return $this->fetch();
        }
    }
}