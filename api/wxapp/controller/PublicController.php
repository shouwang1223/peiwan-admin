<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\wxapp\controller;

use app\admin\model\AreaModel;
use app\admin\model\CouponModel;
use app\admin\model\MemberModel;
use app\admin\model\PlayWithModel;
use cmf\lib\Storage;
use Exception;
use OpenApi\Annotations as OA;
use think\facade\Db;
use cmf\lib\Upload;
use think\facade\Log;
use WeChat\Exceptions\InvalidResponseException;
use WeChat\Exceptions\LocalCacheException;
use WeChat\Script;

header('Access-Control-Allow-Origin:*');
// 响应类型
header('Access-Control-Allow-Methods:*');
// 响应头设置
header('Access-Control-Allow-Headers:*');
error_reporting(0);

class PublicController extends AuthController
{
    public $wx_config;

    public function initialize()
    {
        $plugin_config = cmf_get_option('weipay');

        $this->wx_config = [
            //微信基本信息
            'token' => $plugin_config['wx_token'],
            'appid' => !empty($plugin_config['wx_mp_app_id']) ? $plugin_config['wx_mp_app_id'] : $plugin_config['wx_mini_app_id'],
            'appsecret' => !empty($plugin_config['wx_mp_app_secret']) ? $plugin_config['wx_mp_app_secret'] : $plugin_config['wx_mini_app_secret'],
            'encodingaeskey' => $plugin_config['wx_encodingaeskey'],
            // 配置商户支付参数
            'mch_id' => $plugin_config['wx_mch_id'],
            'mch_key' => $plugin_config['wx_v2_mch_secret_key'],
            // 配置商户支付双向证书目录 （p12 | key,cert 二选一，两者都配置时p12优先）
            //	'ssl_p12'        => __DIR__ . DIRECTORY_SEPARATOR . 'cert' . DIRECTORY_SEPARATOR . '1332187001_20181030_cert.p12',
            'ssl_key' => './upload/' . $plugin_config['wx_mch_secret_cert'],
            'ssl_cer' => './upload/' . $plugin_config['wx_mch_public_cert_path'],
            // 配置缓存目录，需要拥有写权限
            'cache_path' => './wx_cache_path',
        ];
    }

    /**
     * 查询系统配置信息
     * @OA\Get(
     *     tags={"小程序公共模块接口"},
     *     path="/wxapp/public/find_setting",
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function find_setting()
    {
        $info = cmf_get_option('set_config');
        $lat = $this->request->param('lat');
        $lng = $this->request->param('lng');

        if (!empty($info['app_logo'])) $info['app_logo'] = cmf_get_asset_url($info['app_logo']);
        if (!empty($info['agreement'])) $info['agreement'] = cmf_replace_content_file_url(htmlspecialchars_decode($info['agreement']));
        if (!empty($info['order_inform'])) $info['order_inform'] = cmf_replace_content_file_url(htmlspecialchars_decode($info['order_inform']));
        if (!empty($info['privacy_agreement'])) $info['privacy_agreement'] = cmf_replace_content_file_url(htmlspecialchars_decode($info['privacy_agreement']));

        $pw_list = PlayWithModel::field("*,6378.138 * 2 * asin(sqrt(pow(sin((lat * pi() / 180 - " . $lat . " * pi() / 180) / 2),2) + cos(lat * pi() / 180) * cos(" . $lat . " * pi() / 180) * pow(sin((lng * pi() / 180 - " . $lng . " * pi() / 180) / 2),2))) as distance")
            ->where('status', '<>', 0)->select()->toArray();

        $pw_count = 0;
        if (!empty($pw_list)) {
            foreach ($pw_list as $k => $v) {
                if ($v['distance'] < 15) $pw_count++;
            }
        }

        $info['pw_count'] = $pw_count;
        $info['is_status'] = 1;

        $this->success("请求成功！", $info);
    }

    public function getArea()
    {
        $Model = new AreaModel();
        $list = $Model->field('name,code,id')->where(['level' => 0])->select()->toArray();
        foreach ($list as $k => $v) {
            $child = $Model->field('name,code,id')->where(['pid' => $v['id']])->select()->toArray();
            $list[$k]['cityList'] = $child;
            unset($list[$k]['id']);
            foreach ($child as $key => $value) {
                $three_child = $Model->field('name,code')->where(['pid' => $value['id']])->select()->toArray();
                $list[$k]['cityList'][$key]['areaList'] = $three_child;
                unset($list[$k]['cityList'][$key]['id']);
            }
        }
        // return json_encode($list);
        $this->success('获取成功', $list);
    }

    /**
     * 上传图片
     * @OA\Post(
     *     tags={"小程序公共模块接口"},
     *     path="/wxapp/public/upload_asset",
     *      @OA\Parameter(
     *         name="filetype",
     *         in="query",
     *         description="默认image,其他video，audio，file",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function upload_asset()
    {
        if ($this->request->isPost()) {
            session('user.id', 1);
            $uploader = new Upload();
            $fileType = $this->request->param('filetype', 'image');
            $uploader->setFileType($fileType);
            $result = $uploader->upload();
            if ($result === false) {
                $this->error($uploader->getError());
            } else {
                // TODO  增其它文件的处理
                $result['preview_url'] = cmf_get_image_preview_url($result["filepath"]);
                $result['url'] = cmf_get_image_url($result["filepath"]);
                $result['filename'] = $result["name"];
                $this->success('上传成功!', $result);
            }
        }
    }

    /**
     * 查询幻灯片
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\db\exception\DbException
     * @OA\Get(
     *     tags={"小程序公共模块接口"},
     *
     *     path="/wxapp/public/find_slide",
     * 	   @OA\Parameter(
     *         name="slide_id",
     *         in="query",
     *         description="幻灯片分类ID，默认传1，可不传",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function find_slide()
    {
        $data = $this->request->param();

        if (empty($data['slide_id'])) $data['slide_id'] = 1;

        $list = Db::name('slide_item')->field("id,title,image,url")->where(['slide_id' => $data['slide_id'], 'status' => 1])->order('list_order asc')->select()->each(function ($item) {
            $item['image'] = cmf_get_asset_url($item['image']);
            return $item;
        });
        $this->success("请求成功!", $list);
    }

    /**
     * 查询导航列表
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\db\exception\DbException
     * @OA\Get(
     *     tags={"小程序公共模块接口"},
     *     path="/wxapp/public/find_navs",
     *    @OA\Parameter(
     *         name="nav_id",
     *         in="query",
     *         description="导航ID 默认为1",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function find_navs()
    {
        $data = $this->request->param();

        if (empty($data['nav_id'])) $data['nav_id'] = 1;

        $list = Db::name("nav_menu")->where(['nav_id' => $data['nav_id'], 'status' => 1, 'parent_id' => 0])->order('list_order asc')->select()->each(function ($item) {
            if ($item['icon']) $item['icon'] = cmf_get_asset_url($item['icon']);
            return $item;
        });

        $this->success("请求成功！", $list);
    }

    /**
     * 小程序授权登录
     * @throws \WeChat\Exceptions\InvalidDecryptException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @OA\Post(
     *     tags={"小程序公共模块接口"},
     *     path="/wxapp/public/wx_app_login",
     *     @OA\Parameter(
     *         name="code",
     *         in="query",
     *         description="code",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="encrypted_data",
     *         in="query",
     *         description="encrypted_data",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="iv",
     *         in="query",
     *         description="iv",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="p_id",
     *         in="query",
     *         description="p_id",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function wx_app_login()
    {
        $parent_id = $this->request->param('pid', 0);
        $code = $this->request->param('code');

        $plugin_config = cmf_get_option('weipay');

        $wx_config = [
            //微信基本信息
            'token' => $plugin_config['wx_token'],
            'appid' => $plugin_config['wx_mini_app_id'],
            'appsecret' => $plugin_config['wx_mini_app_secret'],
            'encodingaeskey' => $plugin_config['wx_encodingaeskey'],
            // 配置商户支付参数
            'mch_id' => $plugin_config['wx_mch_id'],
            'mch_key' => $plugin_config['wx_v2_mch_secret_key'],
            // 配置商户支付双向证书目录 （p12 | key,cert 二选一，两者都配置时p12优先）
            //	'ssl_p12'        => __DIR__ . DIRECTORY_SEPARATOR . 'cert' . DIRECTORY_SEPARATOR . '1332187001_20181030_cert.p12',
            'ssl_key' => './upload/' . $plugin_config['wx_mch_secret_cert'],
            'ssl_cer' => './upload/' . $plugin_config['wx_mch_public_cert_path'],
            // 配置缓存目录，需要拥有写权限
            'cache_path' => './wx_cache_path',
        ];

        if (empty($code)) $this->error('code不能为空');
        $mini = new \WeMini\Crypt($wx_config);

        $wxUserData = $mini->session($code);
        if (empty($wxUserData) || empty($wxUserData['openid'])) $this->error('登陆失败!');

        $findUserInfo = $this->getUserInfoByOpenid($wxUserData['openid']);
        if (empty($findUserInfo)) {
            Db::name("member")->insert([
                'nickname' => '微信用户_' . (Db::name("member")->max('id') + 1),
                'avatar' => !empty(cmf_get_option('set_config')['app_logo']) ? cmf_get_asset_url(cmf_get_option('set_config')['app_logo']) : 'https://thirdwx.qlogo.cn/mmopen/vi_32/POgEwh4mIHO4nibH0KlMECNjjGxQUq24ZEaGT4poC6icRiccVGKSyXwibcPq4BWmiaIGuG1icwxaQX6grC9VemZoJ8rg/132',
                'openid' => $wxUserData['openid'],
                'parent_id' => $parent_id,
                'create_time' => time(),
            ]);

            if (!empty($parent_id)) {
                $config = cmf_get_option('set_config');
                CouponModel::insert(['user_id' => $parent_id, 'create_time' => time(), 'end_time' => (time() + 86430), 'amount' => $config['coupon_amount']]);
            }
        }
        $info = $this->getUserInfoByOpenid($wxUserData['openid']);
        $this->success("登录成功!", $info);
    }

    /**
     * 小程序授权手机号
     * @throws \WeChat\Exceptions\InvalidDecryptException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @OA\Post(
     *     tags={"小程序公共模块接口"},
     *     path="/wxapp/public/wx_app_phone",
     *     @OA\Parameter(
     *         name="code",
     *         in="query",
     *         description="code",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="encrypted_data",
     *         in="query",
     *         description="encrypted_data",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="iv",
     *         in="query",
     *         description="iv",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function wx_app_phone()
    {
        $data = $this->request->param();
        $parent_id = $this->request->param('pid', 0);

        $check_result = $this->validate($data, 'WxLogin');
        if ($check_result !== true) {
            $this->error($check_result);
        }
        $plugin_config = cmf_get_option('weipay');

        $wx_config = [
            //微信基本信息
            'token' => $plugin_config['wx_token'],
            'appid' => $plugin_config['wx_mini_app_id'],
            'appsecret' => $plugin_config['wx_mini_app_secret'],
            'encodingaeskey' => $plugin_config['wx_encodingaeskey'],
            // 配置商户支付参数
            'mch_id' => $plugin_config['wx_mch_id'],
            'mch_key' => $plugin_config['wx_v2_mch_secret_key'],
            // 配置商户支付双向证书目录 （p12 | key,cert 二选一，两者都配置时p12优先）
            //	'ssl_p12'        => __DIR__ . DIRECTORY_SEPARATOR . 'cert' . DIRECTORY_SEPARATOR . '1332187001_20181030_cert.p12',
            'ssl_key' => './upload/' . $plugin_config['wx_mch_secret_cert'],
            'ssl_cer' => './upload/' . $plugin_config['wx_mch_public_cert_path'],
            // 配置缓存目录，需要拥有写权限
            'cache_path' => './wx_cache_path',
        ];

        $mini = new \WeMini\Crypt($wx_config);

        $wxUserData = $mini->userInfo($data['code'], $data['iv'], $data['encrypted_data']);
        if (empty($wxUserData)) {
            $this->error('授权失败!');
        }

        //授权手机号
        $user_phone = $wxUserData['purePhoneNumber'];

        $findUserInfo = $this->getUserInfoByOpenid($wxUserData['openid']);
        if (empty($findUserInfo)) {
            Db::name("member")->insert([
                'nickname' => '微信用户_' . (Db::name("member")->max('id') + 1),
                'avatar' => !empty(cmf_get_option('set_config')['app_logo']) ? cmf_get_asset_url(cmf_get_option('set_config')['app_logo']) : 'https://thirdwx.qlogo.cn/mmopen/vi_32/POgEwh4mIHO4nibH0KlMECNjjGxQUq24ZEaGT4poC6icRiccVGKSyXwibcPq4BWmiaIGuG1icwxaQX6grC9VemZoJ8rg/132',
                'openid' => $wxUserData['openid'],
                'parent_id' => $parent_id,
                'phone' => $user_phone,
                'create_time' => time(),
            ]);

            if (!empty($parent_id)) {
                $config = cmf_get_option('set_config');
                CouponModel::insert(['user_id' => $parent_id, 'create_time' => time(), 'end_time' => (time() + 86430), 'amount' => $config['coupon_amount']]);
            }
        }
        $info = $this->getUserInfoByOpenid($wxUserData['openid']);
        $this->success("授权成功!", $info);
    }

    /**
     * H5授权登录
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @OA\Post(
     *     tags={"小程序公共模块接口"},
     *     path="/wxapp/public/h5_login",
     *     @OA\Parameter(
     *         name="code",
     *         in="query",
     *         description="code",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="state",
     *         in="query",
     *         description="state",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function h5_login()
    {
        $data = $this->request->param();

        if (empty($data['code'])) $this->error('code不能为空');
        if (empty($data['state'])) $this->error('state不能为空');
        $state_arr = explode('/', $data['state']);
        $http = $state_arr[0] . '//';
        $host = $state_arr[2];

        $parent_id = end($state_arr);

        if (!is_numeric($parent_id)) $parent_id = 0;

        $plugin_config = cmf_get_option('weipay');

        $wx_config = [
            //微信基本信息
            'token' => $plugin_config['wx_token'],
            'appid' => $plugin_config['wx_mp_app_id'],
            'appsecret' => $plugin_config['wx_mp_app_secret'],
            'encodingaeskey' => $plugin_config['wx_encodingaeskey'],
            // 配置商户支付参数
            'mch_id' => $plugin_config['wx_mch_id'],
            'mch_key' => $plugin_config['wx_v2_mch_secret_key'],
            // 配置商户支付双向证书目录 （p12 | key,cert 二选一，两者都配置时p12优先）
            //	'ssl_p12'        => __DIR__ . DIRECTORY_SEPARATOR . 'cert' . DIRECTORY_SEPARATOR . '1332187001_20181030_cert.p12',
            'ssl_key' => './upload/' . $plugin_config['wx_mch_secret_cert'],
            'ssl_cer' => './upload/' . $plugin_config['wx_mch_public_cert_path'],
            // 配置缓存目录，需要拥有写权限
            'cache_path' => './wx_cache_path',
        ];

        $WeChat = new \WeChat\Oauth($wx_config);

        $return = $WeChat->getOauthAccessToken($data['code']);

        if (empty($return)) {
            $this->error('登陆失败!');
        }

        $h5_access_token = $return['access_token'];
        $openid = $return['openid'];

        $UserData = $WeChat->getUserInfo($h5_access_token, $openid);

        $findUserInfo = $this->getUserInfoByOpenid($openid);
        if (empty($findUserInfo)) {
            if (!empty($parent_id)) {
                $config = cmf_get_option('set_config');
                $parent_info = MemberModel::find($parent_id);
                if (!empty($parent_info['type']) && $parent_info['type'] == 1) $parent_id = 0;

                CouponModel::insert(['user_id' => $parent_id, 'create_time' => time(), 'end_time' => (time() + 86430), 'amount' => $config['coupon_amount']]);
            }
            Db::name("member")->insert([
                'nickname' => urldecode($UserData['nickname']),
                'avatar' => $UserData['headimgurl'],
                'openid' => $UserData['openid'],
                'create_time' => time(),
                'parent_id' => $parent_id,
            ]);
        } else {
            Db::name("member")->where('openid', $openid)->update([
                'nickname' => urldecode($UserData['nickname']),
                'avatar' => $UserData['headimgurl'],
            ]);
        }

        header('Location:' . $http . $host . '/h5/#/pages/public/h5login?openid=' . $openid);
    }

    /**
     * 小程序程序二维码
     * @OA\Get(
     *     tags={"小程序公共模块接口"},
     *     path="/wxapp/public/wx_qrcode",
     * 	   @OA\Parameter(
     *         name="scene",
     *         in="query",
     *         description="scene",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function wx_qrcode($scene = '', $page = 'pages/index/index')
    {
        if (empty($scene)) $scene = $this->request->param('scene');

        $path = "upload/wx_qrcode/" . $scene . ".png";
        $rel_path = "wx_qrcode/" . $scene . ".png";

        $url = cmf_get_asset_url($rel_path);
        if (!file_exists($url)) {
            // 实例SDK
            $mini = new \WeMini\Qrcode($this->wx_config);
            try {
                //要打开的小程序版本。正式版为 release，体验版为 trial，开发版为 develop
                $result = $mini->createMiniScene($scene, $page, 'release');
                file_put_contents($path, $result);
                //上传Oss储存
                $storage = cmf_get_option('storage');
                $storage = new Storage($storage['type'], $storage['storages'][$storage['type']]);
                $storage->upload($rel_path, $path);
            } catch (Exception $e) {
                // 出错啦，处理下吧
                $this->error($e->getMessage());
            }
        }

        $this->success("请求成功！", $url);
    }

    /**
     * 获取分享签名
     * @OA\Get(
     *     tags={"小程序公共模块接口"},
     *     path="/wxapp/public/get_js_sign",
     * 	   @OA\Parameter(
     *         name="url",
     *         in="query",
     *         description="url",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function get_js_sign()
    {
        $url = $this->request->param('url');

        $WeChat = new Script($this->wx_config);
        $res = $WeChat->getJsSign(urldecode($url));
        $this->success("请求成功！", $res);
    }

    /**
     * 手机号密码注册
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @OA\Post(
     *     tags={"小程序公共模块接口"},
     *     path="/wxapp/public/add_user_pass",
     *     @OA\Parameter(
     *         name="phone",
     *         in="query",
     *         description="手机号码",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="code",
     *         in="query",
     *         description="验证码",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="pass",
     *         in="query",
     *         description="密码",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function add_user_pass()
    {
        $data = $this->request->param();
        $data['phone'] = trim($data['phone']);
        if (empty($data['phone'])) $this->error("手机号不能为空！");
        if (empty($data['code'])) $this->error("验证码不能为空！");
        if (empty($data['pass'])) $this->error("密码不能为空！");

        $result = cmf_check_verification_code($data['phone'], $data['code']);
        if ($result) {
            $this->error($result);
        } else {
            $findUserInfo = Db::name("member")->where(["phone" => $data['phone']])->find();
            if (empty($findUserInfo)) {
                Db::name('member')->insert(['phone' => $data['phone'], 'pass' => cmf_password($data['pass'])]);
                $this->success("注册成功！");
            } else {
                $this->error("你已经注册过了！");
            }
        }
    }

    /**
     * 手机号密码登录
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\db\exception\DbException
     * @OA\Post(
     *     tags={"小程序公共模块接口"},
     *     path="/wxapp/public/pass_login",
     *     @OA\Parameter(
     *         name="phone",
     *         in="query",
     *         description="手机号码",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="pass",
     *         in="query",
     *         description="密码",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function pass_login()
    {
        $data = $this->request->param();
        $data['phone'] = trim($data['phone']);
        if (empty($data['phone'])) $this->error("手机号不能为空！");
        if (empty($data['pass'])) $this->error("密码不能为空！");

        $findUserInfo = Db::name("member")->where(["phone" => $data['phone']])->find();
        if (empty($findUserInfo)) {
            $this->error("用户不存在，请先注册！");
        }
        if (cmf_compare_password($data['pass'], $findUserInfo['pass'])) {
            $this->success("登录成功！", $findUserInfo);
        } else {
            $this->error("密码错误！");
        }
    }

    /**
     * 手机号验证码登录
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\db\exception\DbException|\think\Exception
     * @OA\Post(
     *     tags={"小程序公共模块接口"},
     *     path="/wxapp/public/sms_login",
     *     @OA\Parameter(
     *         name="phone",
     *         in="query",
     *         description="手机号码",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="code",
     *         in="query",
     *         description="验证码",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function sms_login()
    {
        $data = $this->request->param();
        $data['phone'] = trim($data['phone']);
        if (empty($data['phone'])) $this->error("手机号不能为空！");
        if (empty($data['code'])) $this->error("验证码不能为空！");

        $findUserInfo = Db::name("member")->where(["phone" => $data['phone']])->find();
        if (empty($findUserInfo)) {
            $this->error("用户不存在，请先注册！");
        }
        $result = cmf_check_verification_code($data['phone'], $data['code']);
        if ($result) {
            $this->error($result);
        } else {
            $this->success("登录成功！", $findUserInfo);
        }
    }

    /**
     * 获取手机验证码
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\db\exception\DbException
     * @throws Exception
     * @OA\Get(
     *     tags={"小程序公共模块接口"},
     *     path="/wxapp/public/send_sms",
     *     @OA\Parameter(
     *         name="phone",
     *         in="query",
     *         description="手机号码",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="An example resource"),
     *     @OA\Response(response="default", description="An example resource")
     * )
     */
    public function send_sms()
    {
        $phone = $this->request->param('phone');
        $phone = trim($phone);
        if (empty($phone)) $this->error("手机号不能为空！");

        $ali_sms = cmf_get_plugin_class("AliSms");
        $sms = new $ali_sms();
        $code = cmf_get_verification_code($phone);

        $params = ["mobile" => $phone, "code" => $code];

        cmf_verification_code_log($phone, $code);
        $result = $sms->sendMobileVerificationCode($params);

        if ($result['error'] == 0) {
            $this->success($result['message']);
        } else {
            $this->error($result['message']);
        }
    }
}
