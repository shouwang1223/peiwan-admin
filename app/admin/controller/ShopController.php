<?php
/**
 * Created by PhpStorm.
 * User: zhang
 * Date: 2021/9/25
 * Time: 11:31
 */

namespace app\admin\controller;

use app\admin\model\CategoryModel;
use cmf\controller\AdminBaseController;
use app\admin\model\ShopModel;

class ShopController extends AdminBaseController
{
    /**
     * 显示资源列表
     */
    public function index()
    {
        $keyword = $this->request->param('keyword');
        $cate_id = $this->request->param('cate_id');

        $map = [];
        if (!empty($keyword)) $map[] = ['id|shop_name|province|city|county|address', 'like', "%$keyword%"];
        if (!empty($cate_id)) $map[] = ['cate_id', '=', $cate_id];

        $model = new ShopModel();
        $list = $model->where($map)->order('id desc')->paginate(12)->each(function ($item) {
            $item['cate_name'] = CategoryModel::where(['id' => $item['cate_id']])->value('cate_name');
            return $item;
        });

        $list->appends(['keyword' => $keyword]);
        $page = $list->render();
        $category = CategoryModel::order('id asc')->select();
        $this->assign('category', $category);
        $this->assign('page', $page);
        $this->assign('list', $list);
        return $this->fetch();
    }

    public function add()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();

            $Model = new ShopModel();

            if (empty($data['logo'])) $this->error('请上传LOGO');

            if (empty($data['address']) || empty($data['lat']) || empty($data['lng'])) $this->error('填写地址后请点击搜索');

            if (empty($data['thumb'])) $this->error('请上传轮播图');

            $data['thumb'] = implode(',', $data['thumb']);

            $res = $Model->save($data);
            if ($res) {
                $this->success('添加成功',url('Shop/index'));
            } else {
                $this->error('添加失败');
            }
        } else {
            $category = CategoryModel::order('id asc')->select();
            $this->assign('category', $category);
            return $this->fetch();
        }
    }

    /**
     * 显示编辑资源表单页.
     */
    public function edit()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();

            $Model = new ShopModel();

            $info = $Model->find($data['id']);

            if (empty($info)) $this->error('不存在');

            if (empty($data['logo'])) $this->error('请上传LOGO');

            if ($data['address'] != $info['address'] && $data['lat'] == $info['lat'] && $data['lng'] == $info['lng']) $this->error('修改地址后请点击搜索');

            if (empty($data['thumb'])) $this->error('请上传轮播图');

            $data['thumb'] = implode(',', $data['thumb']);

            $res = $Model->where(['id' => $data['id']])->save($data);
            if ($res) {
                $this->success('保存成功');
            } else {
                $this->error('保存失败');
            }
        } else {
            $shop_id = $this->request->param('id');

            $Model = new ShopModel();

            $info = $Model->find($shop_id);

            $info['thumb'] = explode(',', $info['thumb']);
            $category = CategoryModel::order('id desc')->select();
            $this->assign('category', $category);
            $this->assign('info', $info);
            return $this->fetch();
        }
    }

    /**
     * 删除指定资源
     */
    public function delete()
    {
        if ($this->request->isPost()) {
            $id = $this->request->param('id');
            $info = ShopModel::find($id);
            if (empty($info)) {
                $this->error('不存在');
            } else {
                ShopModel::destroy($id);
                $this->success("删除成功！");
            }
        } else {
            $this->error('非法操作');
        }
    }

    /**
     * 地址转换为坐标
     */
    public function search_address()
    {
        $param = $this->request->param();
        $url = "https://apis.map.qq.com/ws/geocoder/v1/?key=S3GBZ-WG46K-E5IJI-A3G67-E5Y25-MSFA4&address=" . $param['address'];
        $result = file_get_contents($url);
        exit($result);
    }

    /**
     * 地址转换为坐标
     */
    public function reverse_address()
    {
        $param = $this->request->param();
        $url = "https://apis.map.qq.com/ws/geocoder/v1/? location=" . $param['lnglat'] . "&key=S3GBZ-WG46K-E5IJI-A3G67-E5Y25-MSFA4&get_poi=1";
        $result = file_get_contents($url);
        exit($result);
    }
}