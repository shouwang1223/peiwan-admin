<?php
/**
 * Created by PhpStorm.
 * User: zhang
 * Date: 2021/11/29
 * Time: 16:38
 */

namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use think\facade\Db;

class ExcelController extends AdminBaseController
{
	/**
	 * excel导入
	 */
	public function excelImport()
	{
		if ($this->request->isPost()) {
			$my_file = $_FILES['my_file'];
			//获取表格的大小，限制上传表格的大小5M
			if ($my_file['size'] > 5 * 1024 * 1024) {
				$this->error('文件大小不能超过5M');
			}

			//限制上传表格类型
			$fileExtendName = substr(strrchr($my_file["name"], '.'), 1);

			if ($fileExtendName != 'xls' && $fileExtendName != 'xlsx') {
				$this->error('必须为excel表格');
			}

			if (is_uploaded_file($my_file['tmp_name'])) {
				// 有Xls和Xlsx格式两种
				if ($fileExtendName == 'xlsx') {
					$objReader = IOFactory::createReader('Xlsx');
				} else {
					$objReader = IOFactory::createReader('Xls');
				}

				$filename = $my_file['tmp_name'];

				$objPHPExcel = $objReader->load($filename);  	//$filename可以是上传的表格，或者是指定的表格

				$sheet = $objPHPExcel->getSheet(0);   //excel中的第一张sheet

				$highestRow = $sheet->getHighestRow();       	// 取得总行数

				// $highestColumn = $sheet->getHighestColumn(); // 取得总列数

				//循环读取excel表格，整合成数组。如果是不指定key的二维，就用$data[i][j]表示。
				for ($j = 2; $j <= $highestRow; $j++) {
					if (empty($objPHPExcel->getActiveSheet()->getCell("A" . $j)->getValue())) {
						continue;
					}
					$data[$j - 2] = [
						'conclusion' 	=> $objPHPExcel->getActiveSheet()->getCell("A" . $j)->getValue(),
						'number' 		=> $objPHPExcel->getActiveSheet()->getCell("B" . $j)->getValue(),
						'weight' 		=> $objPHPExcel->getActiveSheet()->getCell("C" . $j)->getValue(),
                        'shape' 		=> $objPHPExcel->getActiveSheet()->getCell("D" . $j)->getValue(),
						'color' 		=> $objPHPExcel->getActiveSheet()->getCell("E" . $j)->getValue(),
						'magnification' => $objPHPExcel->getActiveSheet()->getCell("F" . $j)->getValue(),
					];
				}

				Db::name('excel')->insertAll($data);
				$this->success("导入成功！", url('index/index'));
			}
		}
	}

	/**
	 * excel导出
	 */
	public function excelExport()
	{
		//切记只能直接请求，不能使用后台自带的ajax请求

		$list = Db::name('excel')->order(['id' => 'desc'])->select()->toArray();

		//创建一个新的excel文档
		$newExcel = new Spreadsheet();

		//获取当前操作sheet的对象
		$objSheet = $newExcel->getActiveSheet();

		//设置宽度为true,不然太窄了
		$newExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$newExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$newExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$newExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$newExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
		$newExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$newExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$newExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
		$newExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);

		//设置第一栏的标题
		$objSheet->setCellValue('A1', '试卷')
			->setCellValue('B1', '学生姓名')
			->setCellValue('C1', '学校')
			->setCellValue('D1', '年级')
			->setCellValue('E1', '奖项')
			->setCellValue('F1', '总排名')
			->setCellValue('G1', '理论排名')
			->setCellValue('H1', '实验排名')
			->setCellValue('I1', '第一题');

		//第二行起，每一行的值,setCellValueExplicit是用来导出文本格式的。
		//->setCellValueExplicit('C' . $key, $val['admin_password']PHPExcel_Cell_DataType::TYPE_STRING),可以用来导出数字不变格式

		if (!empty($list)) {
			foreach ($list as $key => $val) {
				$key = $key + 2;

				//设置第行高度
				$newExcel->getActiveSheet()->getRowDimension($key)->setRowHeight(20);
				$newExcel->getActiveSheet()->getStyle($key)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);

				//样式设置 - 水平、垂直居中
				$styleArray = [
					'alignment' => [
						'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
						'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
					],
				];
				$newExcel->getActiveSheet()->getStyle('A1:V' . $key)->applyFromArray($styleArray);

				//设置行值
				$objSheet->setCellValue('A' . $key, $val['title'])
					->setCellValue('B' . $key, $val['name'])
					->setCellValue('C' . $key, $val['school_name'])
					->setCellValue('D' . $key, $val['grade'])
					->setCellValue('E' . $key, $val['prize'])
					->setCellValue('F' . $key, $val['total'])
					->setCellValue('G' . $key, $val['theory'])
					->setCellValue('H' . $key, $val['experiment'])
					->setCellValue('I' . $key, !empty($val['subject'][1]) ? $val['subject'][1] : '');
			}
		} else {
			$this->error('暂无数据');
		}

		//设置当前sheet的标题
		$objSheet->setTitle('评测试卷列表');

		//导出
		$filename = '评测试卷';

		$format = 'Xlsx';

		//清空缓冲区并关闭输出缓冲
		ob_end_clean();
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition: attachment;filename=" . $filename . date('Y-m-d') . '.' . strtolower($format));
		header('Cache-Control: max-age=0');
		$objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($newExcel, $format);
		$objWriter->save('php://output');
		exit();
	}

    /**
     * excel
     * @param array $data 数据data
     * @param array $headerRow 首行数据data
     * @param array $conf [fileName] string 文件名 | [format] string 文件格式后缀 xls
     * @param array $subtitle [rowName] string 列名 | [acrossCells] string 跨越列数
     * @return void
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function create(array $data, array $headerRow, array $conf = [], array $subtitle = [])
    {
        //生成文件信息
        if (isset($conf['fileName']) && $conf['fileName']) {
            $fileName = $conf['fileName'];
        } else {
            $fileName = date('YmdHis') . rand(1000, 9999) . rand(1000, 9999);
        }
        $format = $conf['format'] ?? 'Xlsx'; //'Xls'
        $newExcel = new Spreadsheet(); //创建一个新的excel文档

        $objSheet = $newExcel->getActiveSheet(); //获取当前操作sheet的对象
        $objSheet->setTitle($fileName); //设置当前sheet的标题

        $sort = 0;
        //首行标题
        if (isset($conf['fileName']) && $conf['fileName']) {
            //首行标题
            $styleArray = [
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ],
            ];
            $objSheet->setCellValue('A1', $fileName);
            $objSheet->getStyle('A1')->applyFromArray($styleArray);

            $length = count($headerRow) - 1;
            $objSheet->mergeCells('A1:' . $this->intToChr($length) . '1');
            $sort = 1;
        }

        /** 副标题列 */
        if ($subtitle) {
            $subSort = 0;
            foreach ($subtitle as $key) {
                $objSheet->setCellValue($this->intToChr($subSort) . "2", $key['rowName'] ?? ' ');
                $endSort = $subSort + $key['acrossCells'] - 1;
                $objSheet->mergeCells($this->intToChr($subSort) . "2:" . $this->intToChr($endSort) . "2");
                // p($this->intToChr($subSort) . "2:" . $this->intToChr($endSort) . "2", 0);
                $subSort = $endSort + 1;
            }
            $sort = 2;
        }

        /** 字段渲染列 */
        $sort += 1;
        $sheetConfig = false; //根据 $headerRow 配置数据读取方式
        //标题栏设置 & 行宽设置
        $headerRowCount = count($headerRow);
        for ($i = 0; $i < $headerRowCount; $i++) {
            $rowLetter = $this->intToChr($i);
            //设置宽度为true,不然太窄了
            $newExcel->getActiveSheet()->getColumnDimension($rowLetter)->setAutoSize(true);
            //设置第一栏的标题
            if (is_array($headerRow[$i])) {
                $sheetConfig = true;
                $objSheet->setCellValue($rowLetter . $sort, $headerRow[$i]['rowName']);
            } else {
                $objSheet->setCellValue($rowLetter . $sort, $headerRow[$i]);
            }
        }

        //第二行起，每一行的值
        $row_key = $sort + 1;
        $endCell = '';
        if (!$sheetConfig) {
            foreach ($data as $key) {
                foreach ($key as $val) {
                    $rowLetter = $this->intToChr($key);
                    // $objSheet->getColumnDimension($rowLetter)->setAutoSize(true);
                    $objSheet->setCellValue($rowLetter . $row_key, $val);
                }
                $row_key++;
                // $endCell = $rowLetter . $row_key;
            }
        } else {
            foreach ($data as $rowVal) {
                foreach ($headerRow as $key => $val) {
                    $rowLetter = $this->intToChr($key);
                    // $objSheet->getColumnDimension($rowLetter)->setAutoSize(true);
                    $objSheet->setCellValue($rowLetter . $row_key, $rowVal[$val['rowVal']]);
                }
                $row_key++;
                // $endCell = $rowLetter . $row_key;
            }
        }

        // $styleArray = [
        //     'borders' => [
        //         'outline' => [
        //             'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
        //             'color' => ['argb' => '00000000'],
        //         ],
        //         'inline' => [
        //             'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
        //             'color' => ['argb' => '00000000'],
        //         ],
        //     ],
        // ];
        // $objSheet->getStyle("A1:$endCell")->applyFromArray($styleArray);

        // $format只能为 Xlsx 或 Xls
        if ($format == 'Xlsx') {
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        } elseif ($format == 'Xls') {
            header('Content-Type: application/vnd.ms-excel');
        }

        //直接输出文件
        header("Content-Disposition: attachment;filename=" . $fileName . '.' . strtolower($format));
        header('Cache-Control: max-age=0');
        $objWriter = IOFactory::createWriter($newExcel, $format);
        $objWriter->save('php://output');
        exit;
    }

    public function intToChr($num)
    {
        $data = [];
        for ($i = 0; $i <= 701; $i++) {
            $y = ($i / 26);
            if ($y >= 1) {
                $y = intval($y);
                $data[$i] = chr($y + 64) . chr($i - $y * 26 + 65);
            } else {
                $data[$i] = chr($i + 65);
            }
        }
        return $data[$num];
    }
}