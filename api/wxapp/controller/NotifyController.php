<?php
/**
 * Created by PhpStorm.
 * User: zhang
 * Date: 2021/12/17
 * Time: 11:08
 */

namespace api\wxapp\controller;

use cmf\controller\RestBaseController;
use plugins\weipay\lib\PayController;
use think\facade\Db;
use think\facade\Log;

class NotifyController extends RestBaseController
{
    public function wxPayNotify(): string
    {
        $Pay = new PayController();

        $pay_data = $Pay->wx_pay_notify();
        if ($pay_data != 1) Log::write($pay_data['msg'], 'wx_pay_notify');

        $data = $pay_data['data'];

        if ($data['event_type'] === 'TRANSACTION.SUCCESS') {

            Log::write($data, '订单回调');

            $result = $data['resource']['ciphertext'];
            $order_num = $result['out_trade_no'];
            $pay_amount = $result['amount']['total'];

            $order_info = Db::name('pw_order')->where('order_num', $order_num)->find();

            Log::write($order_info, 'order_info');
            if (!empty($order_info)) {
                if ($order_info['status'] == 1) {
                    $amount = $order_info['total_amount'] * 100;
                    if ($amount != $pay_amount) {
                        Log::write('金额错误', $order_num);
                    } else {
                        //支付完成更改订单状态
                        Db::name('pw_order')->where('order_num', $order_num)->update(['status' => 2]);
                        if ($order_info['type'] == 2) {
                            $info = Db::name('pw_order_append')->where('order_num', $order_num)->find();
                            Db::name('pw_order_append')->where('order_num', $order_num)->update(['status' => 2]);
                            Db::name('pw_order')->where('order_num', $info['append_order_num'])->inc('expiration_time', ($info['append_time'] * 3600))->update(['append_order_time' => $info['append_time']]);
                        } elseif ($order_info['type'] == 3) {
                            Db::name('pw_earnings')->insert(['user_id' => $order_info['user_id'], 'type' => 1, 'price' => $order_info['total_amount'], 'create_time' => time()]);
                            Db::name('member')->where('id', $order_info['user_id'])->inc('balance', $order_info['total_amount'])->update();
                        } elseif ($order_info['type'] == 4) {
                            $info = Db::name('member')->where('id', $order_info['user_id'])->find();
                            if ($info['member_time'] > time()) {
                                $time = $info['member_time'] + 2592000;
                            } else {
                                $time = time() + 2592000;
                            }
                            Db::name('member')->where('id', $order_info['user_id'])->update(['member_time' => $time]);
                        } else {
                            $send = new CommonController();
                            $send->newOrderSendSms($order_num);
                        }
                        Log::write('订单支付成功', $order_num);
                    }
                    ob_clean();
                    return $Pay->wx_pay_success();
                } else {
                    Log::write('订单状态错误', $order_num);
                }
            } else {
                Log::write('订单不存在', $order_num);
            }
        } else {
            Log::write('event_type:' . $data['event_type']);
        }
    }

    public function returnCallback()
    {
        $data = $this->request->param();
        Log::write($data, 'returnCallback');
    }

    public function aliPayNotify(): string
    {
        $Pay = new PayController();

        $pay_data = $Pay->ali_pay_notify();
        if ($pay_data != 1) Log::write($pay_data['msg'], 'wx_pay_notify');

        $data = $pay_data['date'];

        if (in_array($data['trade_status'], ['TRADE_SUCCESS', 'TRADE_FINISHED'])) {

            Log::write($data, '订单回调');

            $order_num = $data['out_trade_no'];
            $pay_amount = $data['total_amount'];

            $order_info = Db::name('shop_order')->where('order_num', $order_num)->find();

            Log::write($order_info, 'order_info');
            if (!empty($order_info)) {
                if ($order_info['status'] == 1) {
                    if ($order_info['amount'] != $pay_amount) {
                        Log::write('金额错误', $order_num);
                    } else {
                        //支付完成更改订单状态
                        Log::write('订单支付成功', $order_num);
                    }
                    ob_clean();
                    return $Pay->ali_pay_success();
                } else {
                    Log::write('订单状态错误', $order_num);
                }
            } else {
                Log::write('订单不存在', $order_num);
            }
        } else {
            Log::write('trade_status:' . $data['trade_status']);
        }
    }
}