<?php
namespace app\admin\controller;

use app\admin\model\TagModel;
use cmf\controller\AdminBaseController;
use Mpdf\Tag\A;

class TagController extends AdminBaseController
{
    public function onlineList()
    {
        $model = new TagModel();
        $list = $model->where(['pid'=>1])->order('id desc')->paginate(12);
//        var_dump($list);exit;
        $page = $list->render();
        $this->assign('list', $list);
        $this->assign('page', $page);
        return $this->fetch();

    }
    public function onlineAdd()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();

            $Model = new TagModel();

            $data['pid'] = 1;

            $res = $Model->save($data);
            if ($res) {
                $this->success('添加成功',url('tag/onlineList'));
            } else {
                $this->error('添加失败');
            }
        } else {
            return $this->fetch();
        }

    }

    public function onlineEdit()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();

            $Model = new TagModel();

            $info = $Model->find($data['id']);

            if (empty($info)) $this->error('不存在');

            $res = $Model->where(['id' => $data['id']])->save($data);
            if ($res) {
                $this->success('保存成功');
            } else {
                $this->error('保存失败');
            }
        } else {
            $id = $this->request->param('id');

            $Model = new TagModel();
            $info = $Model->find($id);
            $this->assign('info', $info);
            return $this->fetch();
        }
    }

    public function onlineDelete()
    {
        if ($this->request->isPost()) {
            $id = $this->request->param('id');
            $info = TagModel::find($id);
            if (empty($info)) {
                $this->error('不存在');
            } else {
                TagModel::destroy($id);
                $this->success("删除成功！");
            }
        } else {
            $this->error('非法操作');
        }
    }

    public function offlineList()
    {
        $model = new TagModel();
        $list = $model->where(['pid'=>2])->order('id desc')->paginate(12);
//        var_dump($list);exit;
        $page = $list->render();
        $this->assign('list', $list);
        $this->assign('page', $page);
        return $this->fetch();

    }
    public function offlineAdd()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();

            $Model = new TagModel();

            $data['pid'] = 2;

            $res = $Model->save($data);
            if ($res) {
                $this->success('添加成功',url('tag/offlineList'));
            } else {
                $this->error('添加失败');
            }
        } else {
            return $this->fetch();
        }

    }

    public function offlineEdit()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();

            $Model = new TagModel();

            $info = $Model->find($data['id']);

            if (empty($info)) $this->error('不存在');

            $res = $Model->where(['id' => $data['id']])->save($data);
            if ($res) {
                $this->success('保存成功');
            } else {
                $this->error('保存失败');
            }
        } else {
            $id = $this->request->param('id');

            $Model = new TagModel();
            $info = $Model->find($id);
            $this->assign('info', $info);
            return $this->fetch();
        }
    }

    public function offlineDelete()
    {
        if ($this->request->isPost()) {
            $id = $this->request->param('id');
            $info = TagModel::find($id);
            if (empty($info)) {
                $this->error('不存在');
            } else {
                TagModel::destroy($id);
                $this->success("删除成功！");
            }
        } else {
            $this->error('非法操作');
        }
    }

}
