<?php
/**
 * Created by PhpStorm.
 * User: zhang
 * Date: 2021/12/9
 * Time: 10:11
 */

namespace app\admin\controller;

use app\admin\model\MemberModel;
use app\admin\model\WithdrawModel;
use cmf\controller\AdminBaseController;

class WithdrawController extends AdminBaseController
{
    /**
     * 显示资源列表
     */
    public function index()
    {
        $keyword = $this->request->param('keyword');
        $map = [];
        if (!empty($keyword)) $map[] = ['b.nickname|a.zfb_num', 'like', "%$keyword%"];

        $Model = new WithdrawModel();

        $list = $Model->alias('a')->join('member b', 'a.user_id = b.id')->field('a.*,b.nickname')->where($map)->order(['id' => 'DESC'])->paginate(12);

        $list->appends(['keyword' => $keyword]);
        $page = $list->render();
        $this->assign('page', $page);
        $this->assign('list', $list);
        return $this->fetch();
    }

    /**
     * 显示编辑资源表单页.
     */
    public function update()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();

            $Model = new WithdrawModel();

            $info = $Model->find($data['id']);

            if (empty($info)) $this->error('不存在');

            if ($info['status'] != 1) $this->error('发放失败');

            $res = $Model->where(['id' => $data['id']])->update(['status' => $data['status'], 'audit_time' => time()]);
            if ($res) {
                if ($data['status'] == 3) MemberModel::where('id', $info['user_id'])->inc('balance', $info['amount'])->update();
                $this->success('操作成功');
            } else {
                $this->error('操作失败');
            }
        }
    }

    /**
     * 删除指定资源
     */
    public function delete()
    {
        if ($this->request->isPost()) {
            $id = $this->request->param('id');

            $info = WithdrawModel::find($id);
            if (empty($info)) {
                $this->error('不存在');
            } else {
                WithdrawModel::destroy($id);
                $this->success("删除成功！");
            }
        } else {
            $this->error('非法操作');
        }
    }
}